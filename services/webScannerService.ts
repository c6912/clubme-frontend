import axios from "axios";
import { getUser } from "../utils/token";
const baseUrl = "http://193.106.55.117:9090/webscanner";
//const baseUrl = "http://localhost:9090/webscanner";

let headers: any = { Authorization: `Bearer ` };
getUser()
  .then(({ token }) => {
    headers = { Authorization: `Bearer ${token}` };
  })
  .catch();

export default {
  getDreamCardCoupons: () => {
    return new Promise((resolve, reject) => {
      axios({
        method: "get",
        url: `${baseUrl}/coupons`,
        headers,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  getHeverCoupons: () => {
    return new Promise((resolve, reject) => {
      axios({
        method: "get",
        url: `${baseUrl}/coupons/heverGiftCards`,
        headers,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
