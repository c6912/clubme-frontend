// axios
import type { AxiosInstance } from "axios";
import { createAxiosInstance } from "../config/axios/axiosInstance";

const axiosInstance: AxiosInstance = createAxiosInstance({
  //serviceBaseUrl: "http://localhost:2500",
  serviceBaseUrl: "http://193.106.55.117:2500",
  prefix: "/api/coupons",
});

export default {
  getAll: async () => {
    try {
      const { data } = await axiosInstance.get("/");
      return data;
    } catch (err: any) {
      throw err;
    }
  },
  //   getOne: async (id: number): Promise<ICategory> => {
  //     try {
  //       const { data } = await axiosInstance.get<ICategory>(`/${id}`)
  //       return data
  //     } catch (err: any) {
  //       throw err;
  //     }
  //   },
};
