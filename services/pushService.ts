import axios, { AxiosInstance } from "axios";
import { getUser } from "../utils/token";
const usersUrl = `http://193.106.55.117:9090/users`;
//const usersUrl = `http://localhost:9090/users`;

let headers: any = { Authorization: `Bearer ` };
getUser()
  .then(({ token }) => {
    headers = { Authorization: `Bearer ${token}` };
  })
  .catch();

export default {
  updatePushToken: (pushToken: string, username: string) => {
    console.log({ pushToken });
    return new Promise((resolve, reject) => {
      axios({
        method: "put",
        url: `${usersUrl}/push-token`,
        data: { pushToken },
        headers: {
          ...headers,
          "clubme-user-id": username,
        },
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
