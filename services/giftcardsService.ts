import axios from "axios";
const baseUrl = "http://193.106.55.117:9090/common/giftcards";
//const baseUrl = "http://localhost:9090/common/giftcards";

export default {
  getGiftCards: () => {
    return new Promise((resolve, reject) => {
      axios({
        method: "get",
        url: `${baseUrl}/`,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
