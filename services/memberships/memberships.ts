// axios
import type { AxiosInstance } from "axios";
import { createAxiosInstance } from "./../axiosInstance/axiosConfig";
import { backendBaseUrl } from "../../utils/config";
import { getUser } from "../../utils/token";

let headers: any = { Authorization: `Bearer ` };
getUser().then(({ token }) => {
	headers = { Authorization: `Bearer ${token}` };
}).catch();

const axiosInstance: AxiosInstance = createAxiosInstance({
	serviceBaseUrl: backendBaseUrl!,
	prefix: "/membership-cards",
});

export default {
	deleteById: async (id: number) => {
		try {
			const { data } = await axiosInstance.delete(`/${id}`);
			return data;
		} catch (err: any) {
			throw err;
		}
	},

	getUserMembershipCards: async (userName: string) => {
		try {
			const { data } = await axiosInstance.get(`/${userName}`);
			return data;
		} catch (err: any) {
			throw err;
		}
	},
};
