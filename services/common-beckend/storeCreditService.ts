import axios from "axios";
import { backendBaseUrl } from "../../utils/config";
import { getUser } from "../../utils/token";

let headers: any = { Authorization: `Bearer ` };
headers = { Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJlOGFjZGQxZi1mNzhkLTQxYzEtOWQ1ZS05M2RiMzFkZjNmYjgiLCJpYXQiOjE2NTUwNjEwNDN9.-3eu0zoxxmKXbkLXNAwWdxGEMdKwnPo0jdpzdZ62pHE` };

export default {
	create: (data: {
		email: string;
		amount: number;
		store: string;
		name: string;
		purchaseDate: Date;
		expirationDate: Date;
		categories: boolean[];
	}) => {
		return new Promise((resolve, reject) => {
			axios({
				method: "post",
				url: `${backendBaseUrl}/store-credits/`,
				data,
				headers,
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	update: (data: {
		amount: number;
		store: string;
		name: string;
		purchaseDate: Date;
		expirationDate: Date;
	}, email: string,
	) => {
		return new Promise((resolve, reject) => {
			axios({
				method: "put",
				url: `${backendBaseUrl}/store-credits/${email}`,
				data,
				headers,
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},

	delete: (userEmail: string) => {
		return new Promise(async (resolve, reject) => {
			axios({
				method: "delete",
				url: `${backendBaseUrl}/store-credits/${userEmail}`,
				headers: {
					...headers,
					"Access-Control-Allow-Origin": "*",
					"Content-Type": "application/json",
				},
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
};
