import axios, { AxiosInstance } from "axios";
import { backendBaseUrl } from "../../utils/config";
import { getUser } from "../../utils/token";
const usersUrl = `${backendBaseUrl}/users`;

let headers: any = { Authorization: `Bearer ` };
getUser()
	.then(({ token }) => {
		headers = { Authorization: `Bearer ${token}` };
	})
	.catch();

export default {
	find: (email: string) => {
		return new Promise(async (resolve, reject) => {
			axios({
				method: "get",
				url: `${usersUrl}/${email}`,
				headers,
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	findById: (id: string) => {
		return new Promise(async (resolve, reject) => {
			axios({
				method: "get",
				url: `${usersUrl}/userId/${id}`,
				headers,
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	recommendations: (email: string) => {
		return new Promise(async (resolve, reject) => {
			axios({
				method: "get",
				url: `${usersUrl}/recommendations/${email}`,
				headers,
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	delete: (email: string) => {
		return new Promise((resolve, reject) => {
			axios({
				method: "post",
				url: `${usersUrl}/${email}`,
				headers,
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
	notifications: (
		email: string,
		emailEnabled: boolean,
		pushNotificationEnabled: boolean
	) => {
		return new Promise((resolve, reject) => {
			axios({
				method: "put",
				url: `${usersUrl}/notifications`,
				data: { email, emailEnabled, pushNotificationEnabled },
				headers,
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
};
