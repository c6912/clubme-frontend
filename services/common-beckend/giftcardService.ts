import axios from "axios";
import { backendBaseUrl } from "../../utils/config";
import { getUser } from "../../utils/token";

let headers: any = { Authorization: `Bearer ` };
getUser()
  .then(({ token }) => {
    headers = { Authorization: `Bearer ${token}` };
  })
  .catch();

export default {
  create: (data: {
    email: string;
    amount: number;
    store: string;
    name: string;
    purchaseDate: Date;
    expirationDate: Date;
    categories: boolean[];
  }) => {
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url: `${backendBaseUrl}/giftcards/`,
        data,
        headers,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  update: (
    data: {
      points: number;
      store: string;
      name: string;
      purchaseDate: Date;
      expirationDate: Date;
    },
    email: string
  ) => {
    return new Promise((resolve, reject) => {
      axios({
        method: "put",
        url: `${backendBaseUrl}/giftcards/${email}`,
        data,
        headers,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  delete: (userEmail: string) => {
    return new Promise(async (resolve, reject) => {
      axios({
        method: "delete",
        url: `${backendBaseUrl}/giftcards/${userEmail}`,
        headers: {
          ...headers,
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
