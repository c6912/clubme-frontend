import axios from "axios";
import {
	CardType,
	CardTypes,
} from "../../components/PopupCard/PopupCard.types";
import { backendBaseUrl } from "../../utils/config";
import { getUser } from "../../utils/token";

let headers: any = { Authorization: `Bearer ` };
getUser().then(({ token }) => {
	headers = { Authorization: `Bearer ${token}` };
}).catch();

const apiByCardType = {
	[CardTypes.GiftCard]: "giftcardUser",
	[CardTypes.MembershipCard]: "membershipCardsUsers",
	[CardTypes.StoreCredit]: "storeCreditUsers",
};

export default {
	shareCard: (emailToShareWith: string, cardId: string, cardType: CardType) => {
		return new Promise(async (resolve, reject) => {
			axios({
				method: "post",
				url: `${backendBaseUrl}/${apiByCardType[cardType]}/share`,
				data: {
					email: emailToShareWith,
					cardId,
				},
				headers: {
					...headers,
					"Access-Control-Allow-Origin": "*",
					"Content-Type": "application/json",
				},
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},

	getMySharedCards: (userEmail: string) => {
		return new Promise(async (resolve, reject) => {
			axios({
				method: "get",
				url: `${backendBaseUrl}/users/sharedCards/${userEmail}`,
				headers: {
					...headers,
					"Access-Control-Allow-Origin": "*",
					"Content-Type": "application/json",
				},
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
};
