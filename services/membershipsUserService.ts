import axios from "axios";
import { backendBaseUrl } from "../utils/config";
import { getUser } from "../utils/token";

let headers: any = { Authorization: `Bearer ` };
getUser().then(({ token }) => {
	headers = { Authorization: `Bearer ${token}` };
}).catch();
export default {
	setFavorite: (email: string, cardId: string, isFavorite: boolean) => {
		return new Promise(async (resolve, reject) => {
			axios({
				method: "put",
				url: `${backendBaseUrl}/membershipCardsUsers/favorite`,
				data: { cardId, email, isFavorite },
				headers: {
					...headers,
					"Access-Control-Allow-Origin": "*",
					"Content-Type": "application/json",
				},
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
};
