import axios from "axios";
import type {
  AxiosRequestHeaders,
  AxiosInstance,
} from "axios";

export interface IAxiosConfig {
    serviceBaseUrl: string;
    prefix: string;
    headers?: AxiosRequestHeaders;
    isTokenRequired?: boolean;
  }

  
export const createAxiosInstance = ({
  serviceBaseUrl,
  prefix,
}: IAxiosConfig): AxiosInstance => {
  const axiosInstance = axios.create({ baseURL: serviceBaseUrl + prefix });
  return axiosInstance;
};