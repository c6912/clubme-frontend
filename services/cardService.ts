import axios from "axios";
import { backendBaseUrl } from "../utils/config";
import { getUser } from "../utils/token";

let headers: any = { Authorization: `Bearer ` };
getUser().then(({ token }) => {
	headers = { Authorization: `Bearer ${token}` };
}).catch()

export default {
	getCards: (email: string) => {
		return new Promise((resolve, reject) => {
			axios({
				method: "get",
				url: `${backendBaseUrl}/users/cards/${email}`,
				headers,
			})
				.then((response) => {
					resolve(response.data);
				})
				.catch((err) => {
					reject(err);
				});
		});
	},
};
