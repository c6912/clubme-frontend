// axios
import type { AxiosInstance } from "axios";
import { createAxiosInstance } from "./../axiosInstance/axiosConfig";
import { backendBaseUrl } from "../../utils/config";

const axiosInstance: AxiosInstance = createAxiosInstance({
  serviceBaseUrl: backendBaseUrl!,
  prefix: "/store-credits",
});

export default {
  deleteById: async (id: number) => {
    try {
      const { data } = await axiosInstance.delete(`/${id}`)
      return data
    } catch (err: any) {
      throw err;
    }
  },
  getUserStoreCredits: async (userName: string) => {
    try {
      const { data } = await axiosInstance.get(`/${userName}`)
      return data
    }
    catch (err: any) {
      throw err;
    }
  }
};
