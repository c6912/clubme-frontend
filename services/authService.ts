import axios from "axios";
import { GENDER } from "../constants/Gender";
const baseUrl = "http://193.106.55.117:9090/users/auth";
//const baseUrl = "http://localhost:9090/users/auth";

export default {
  login: (username: string, password: string) => {
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url: `${baseUrl}/login`,
        data: { username, password },
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  register: (
    username: string,
    password: string,
    firstName: string,
    lastName: string,
    dateOfBirth: string,
    city: string = "",
    gender: GENDER,
    phone: string = ""
  ) => {
    return new Promise((resolve, reject) => {
      console.log({ gender });
      axios({
        method: "post",
        url: `${baseUrl}/register`,
        data: {
          username,
          password,
          firstName,
          lastName,
          dateOfBirth,
          city,
          gender,
          phone,
        },
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
