import { Searchbar } from "react-native-paper";
import FilterModal from "../../components/popups/filterModel";

import { Card } from "../../components/MenuCard/MenuCard";
import React, { useEffect, useState } from "react";
import {
	Text,
	FlatList,
	TouchableOpacity,
	Dimensions,
	StyleSheet,
	View,
} from "react-native";
import { getUser } from "../../utils/token";
import { CardDetailsModal } from "../../components/popups/CardDetailsModal";
import { CardTypes, Item } from "../../components/PopupCard/PopupCard.types";
import { useMembershipCards } from "../../context/MembershipCards/membershipcards-context";
import membershipsUserService from "../../services/membershipsUserService";
import AddMembershipModal from "../../components/popups/addModals/addMembershipCardModal";
import { CARD_BELONGING_TYPES, CATEGORIES } from "../../constants/Categories";

import { useIsAfterFirstRender } from "../../hooks/useIsAfterFirstRender";

export const MembershipCards = () => {
	const { membershipCards, setMembershipCards } = useMembershipCards();
	const [filteredMembershipCards, setFilteredMembershipCards] =
		useState(membershipCards);
	const [cardModalOpen, setCardModalOpen] = useState(false);
	const [searchQuery, setSearchQuery] = React.useState("");
	const [activeCardIndex, setActiveCardIndex] = useState(null);

	const isAfterFirstRender = useIsAfterFirstRender();

	const handleClose = () => {
		setActiveCardIndex(null);
		setCardModalOpen(false);
	};
	useEffect(() => {
		handleFilterMemberships(new Array(CATEGORIES.length).fill(true), [
			true,
			false,
			false,
		]);
	}, [membershipCards]);

	const handleFilterMemberships = (
		checkedState: boolean[],
		checkedCardType: boolean[]
	) => {
		const filteredByCategories = membershipCards.filter((membership) => {
			const categories = membership?.["categories"] as Array<any>;
			let isOk = false;
			isOk = categories?.some((category) => checkedState[category?.["id"]]);
			if (isOk) return isOk;
			categories.forEach((element, index) => {
				if (element && checkedState[index]) {
					isOk = true;
					return;
				}
			});
			return isOk;
		});
		const filteredByBelonging = filteredByCategories.filter((membership) => {
			if (
				membership?.["shared"] &&
				checkedCardType[CARD_BELONGING_TYPES.SHARED.id]
			)
				return true;
			if (
				!membership?.["shared"] &&
				checkedCardType[CARD_BELONGING_TYPES.MINE.id]
			)
				return true;
			return false;
		});
		setFilteredMembershipCards(filteredByBelonging);
	};

	const onChangeSearch = (query: string) => {
		setSearchQuery(query);

		const filteredMemberships = membershipCards.filter(
			(membershipCard: any) =>
				membershipCard.name.includes(query) ||
				membershipCard.store.includes(query)
		);
		setFilteredMembershipCards(filteredMemberships);
	};

	const renderItem = ({ item, index }: { item: Item; index: number }) => {
		return (
			<>
				<TouchableOpacity
					onPress={() => {
						setCardModalOpen(true);
						setActiveCardIndex(index);
					}}
				>
					<Card
						type={CardTypes.MembershipCard}
						title={item.name}
						store={item.store}
						isFavorite={item.isFavorite}
						shared={item.shared}
						onFavoriteClick={() => {
							setFilteredMembershipCards((prevGiftCards) => {
								const newGiftCards = [...prevGiftCards];
								newGiftCards[index].isFavorite =
									!newGiftCards[index].isFavorite;
								return newGiftCards;
							});
							setMembershipCards((prevGiftCards) =>
								prevGiftCards.map((card) =>
									card.id === item.id
										? { ...card, isFavorite: item.isFavorite }
										: card
								)
							);
							getUser().then(({ username }) => {
								membershipsUserService.setFavorite(
									username,
									item.id,
									filteredMembershipCards[index].isFavorite
								);
							});
						}}
					/>
				</TouchableOpacity>
			</>
		);
	};

	return (
		<>
			<View
				style={{
					width: Dimensions.get("window").width - 30,
					display: "flex",
					flexDirection: "row",
					justifyContent: "space-between",
					alignItems: "center",
					backgroundColor: "white",
				}}
			>
				<Searchbar
					style={styles.input}
					placeholder="Search"
					onChangeText={onChangeSearch}
					value={searchQuery}
				/>
				<View style={{ flex: "0 0 100px", marginRight: 12 }}>
					<FilterModal handleFilterCards={handleFilterMemberships} />
				</View>
			</View>
			{filteredMembershipCards.length ? (
				<View
					style={{
						overflowY: "auto",
						height: 500,
					}}
				>
					<FlatList
						numColumns={2}
						data={filteredMembershipCards}
						extraData={filteredMembershipCards}
						renderItem={renderItem}
						keyExtractor={(item, index) => item.id + index}
					/>
				</View>
			) : (
				<Text>No Membership cards found</Text>
			)}
			<View
				style={{
					position: "absolute",
					top: Dimensions.get("window").height - 100,
				}}
			>
				<AddMembershipModal />
			</View>
			{cardModalOpen && (
				<CardDetailsModal
					{...filteredMembershipCards[activeCardIndex]}
					isSharedCard={filteredMembershipCards[activeCardIndex]?.["shared"]}
					type={CardTypes.MembershipCard}
					open={cardModalOpen}
					onClose={() => handleClose()}
				/>
			)}
		</>
	);
};

const styles = StyleSheet.create({
	item: {
		display: "flex",
		justifyContent: "space-around",
		backgroundColor: "#dddddd",
		padding: 5,
		width: (Dimensions.get("window").width - 50) / 2,
		height: 100,
		marginVertical: 3,
		marginHorizontal: 3,
		borderRadius: 10,
	},
	title: {
		fontSize: 20,
		color: "#586589",
	},
	dropdown: {
		// marginLeft: "15%"
	},
	input: {
		height: 40,
		margin: 12,
		borderWidth: 1,
		borderColor: "rgba(0, 0, 0, 0.2)",
		borderRadius: 100,
		padding: 10,
		width: 200,
		// shadowColor: "none",
	},
});
