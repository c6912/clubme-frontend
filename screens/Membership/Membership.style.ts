import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
	item: {
		display: "flex",
		justifyContent: "space-around",
		backgroundColor: "#dddddd",
		padding: 5,
		width: (Dimensions.get("window").width - 50) / 2,
		height: 100,
		marginVertical: 3,
		marginHorizontal: 3,
		borderRadius: 10,
	},
	title: {
		fontSize: 20,
		color: "#586589",
	},
	input: {
		height: 40,
		margin: 12,
		borderWidth: 1,
		borderColor: "rgba(0, 0, 0, 0.2)",
		borderRadius: 100,
		padding: 10,
		width: 200,
	},
});
