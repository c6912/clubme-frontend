import React, { useState } from "react";
// Components
import { Pressable, Text, TouchableOpacity, View } from "react-native";
import { HelperText, TextInput } from "react-native-paper";
// Services
import authService from "../../services/authService";
// Constants
import { GENDER } from "../../constants/Gender";
// Style
import { styles } from "./SignUpScreen.style";
// Utils
import { useSignUpFields } from "../../hooks/useSignUpFields";
import { SIGN_UP_FIELDS } from "../../constants/SignUpFields";
// import { schema } from "../../utils/signUpValidation";
import { TextField } from "../../components/TextField/TextField";

export default function SignUpScreen({
  redirectToLogin = () => {},
  onRegisterCompleted = () => {},
}: any) {
  const {
    emailAddress,
    password,
    firstName,
    lastName,
    dateOfBirth,
    gender,
    setValue,
    setError,
  } = useSignUpFields();
  const [passwordVisible, setPasswordVisible] = useState(true);

  return (
    <View style={styles.container}>
      <Text style={styles.mainTitle}>BECOME A MEMBER</Text>
      <Text style={styles.subTitle}>
        Create your ClubMe profile and make your life easier! use your benefits
        better and start saving today
      </Text>
      <TextField
        value={emailAddress.value}
        style={styles.input}
        error={emailAddress.error}
        errorStyle={styles.helperText}
        errorMsg="Email address is invalid!"
        label="Email address"
        onChangeText={(username) => {
          setValue(SIGN_UP_FIELDS.EMAIL_ADDRESS, username);
        }}
      />
      <TextInput
        value={password.value}
        style={styles.input}
        onChangeText={(password) => {
          setValue(SIGN_UP_FIELDS.PASSWORD, password);
        }}
        label="Password"
        secureTextEntry={passwordVisible}
        right={
          <TextInput.Icon
            name={passwordVisible ? "eye" : "eye-off"}
            onPress={() => setPasswordVisible(!passwordVisible)}
          />
        }
        autoComplete={false}
      />
      {password.error && (
        <HelperText
          style={styles.helperText}
          type="error"
          visible={password.error}
        >
          Email address is invalid!
        </HelperText>
      )}
      <TextField
        value={firstName.value}
        style={styles.input}
        error={firstName.error}
        errorStyle={styles.helperText}
        errorMsg="First Name is invalid!"
        label="First Name"
        onChangeText={(firstName) => {
          setValue(SIGN_UP_FIELDS.FIRST_NAME, firstName);
        }}
      />
      <TextField
        value={lastName.value}
        style={styles.input}
        error={lastName.error}
        errorStyle={styles.helperText}
        errorMsg="Last Name is invalid!"
        label="Last Name"
        onChangeText={(lastName) => {
          setValue(SIGN_UP_FIELDS.LAST_NAME, lastName);
        }}
      />
      <TextInput
        value={dateOfBirth.value}
        style={styles.input}
        onChangeText={(dateOfBirth) => {
          setValue(SIGN_UP_FIELDS.DATE_OF_BIRTH, dateOfBirth);
        }}
        label="Date of Birth (dd/mm/yyyy)"
        autoComplete={false}
      />
      {dateOfBirth.error && (
        <HelperText
          style={styles.helperText}
          type="error"
          visible={dateOfBirth.error}
        >
          Date of birth is invalid!
        </HelperText>
      )}
      <View style={styles.genderBoxContainer}>
        <TouchableOpacity
          style={
            gender.value === GENDER.MALE
              ? styles.chosenGender
              : styles.genderBox
          }
          onPress={() => {
            setValue(SIGN_UP_FIELDS.GENDER, GENDER.MALE);
          }}
        >
          <Text
            style={
              gender.value === GENDER.MALE
                ? styles.chosenGenderText
                : styles.genderText
            }
          >
            Male
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            gender.value === GENDER.FEMALE
              ? styles.chosenGender
              : styles.genderBox
          }
          onPress={() => {
            setValue(SIGN_UP_FIELDS.GENDER, GENDER.FEMALE);
          }}
        >
          <Text
            style={
              gender.value === GENDER.FEMALE
                ? styles.chosenGenderText
                : styles.genderText
            }
          >
            Female
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            gender.value === GENDER.OTHER
              ? styles.chosenGender
              : styles.genderBox
          }
          onPress={() => {
            setValue(SIGN_UP_FIELDS.GENDER, GENDER.OTHER);
          }}
        >
          <Text
            style={
              gender.value === GENDER.OTHER
                ? styles.chosenGenderText
                : styles.genderText
            }
          >
            Other
          </Text>
        </TouchableOpacity>
      </View>
      <Pressable
        style={styles.register}
        onPress={() => {
          const isEmailValid =
            /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
              emailAddress.value as string
            );
          const isPassValid = password.value?.length >= 3;
          const isGenderValid = gender.value !== GENDER.NONE;
          const isFirstNameValid = firstName.value?.length >= 1;
          const isLastNameValid = lastName.value?.length >= 1;
          const isDateOfBirthValid =
            /(((0|1)[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/((19|20)\d\d))$/.test(
              dateOfBirth.value as string
            );

          console.log({ isEmailValid });
          console.log({ isPassValid });
          console.log({ isGenderValid });
          console.log({ isFirstNameValid });
          console.log({ isLastNameValid });
          console.log({ isDateOfBirthValid });
          setError(SIGN_UP_FIELDS.EMAIL_ADDRESS, !isEmailValid);
          setError(SIGN_UP_FIELDS.PASSWORD, !isPassValid);
          setError(SIGN_UP_FIELDS.FIRST_NAME, !isFirstNameValid);
          setError(SIGN_UP_FIELDS.LAST_NAME, !isLastNameValid);
          setError(SIGN_UP_FIELDS.DATE_OF_BIRTH, !isDateOfBirthValid);
          setError(SIGN_UP_FIELDS.GENDER, !isGenderValid);
          const error =
            !isEmailValid ||
            !isPassValid ||
            !isFirstNameValid ||
            !isLastNameValid ||
            !isDateOfBirthValid ||
            !isGenderValid;
          if (!error) {
            authService
              .register(
                emailAddress.value as string,
                password.value as string,
                firstName.value as string,
                lastName.value as string,
                dateOfBirth.value as string,
                "",
                gender.value as GENDER,
                ""
              )
              .then(({ token }: any) => {
                onRegisterCompleted({
                  username: emailAddress.value,
                  token,
                });
              })
              .catch((error) => {
                console.error(error);
              });
          }
        }}
      >
        <Text style={styles.signUpText}>Sign up</Text>
      </Pressable>
      <View style={styles.signInContainer}>
        <Text style={styles.signInQuestion}>Already have an account?</Text>
        <TouchableOpacity
          onPress={() => {
            redirectToLogin();
          }}
        >
          <Text style={styles.signInText}>Sign In</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
