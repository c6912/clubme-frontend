import React, { useState } from "react";
import {
	Dimensions,
	Pressable,
	StyleSheet,
	Text,
	TouchableOpacity,
} from "react-native";
import { TextInput } from "react-native-paper";

import { View } from "../components/Themed";
import authService from "../services/authService";

export default function LoginScreen({
	redirectToRegister = () => {},
	onLoginCompleted = () => {},
}: any) {
	const [isErrorOn, setIsErrorOn] = useState(false);
	const [username, setUsername] = useState<string>("");
	const [password, setPassword] = useState<string>("");
	const [passwordVisible, setPasswordVisible] = useState(true);

	return (
		<View style={styles.container}>
			<TextInput
				value={username}
				style={styles.input}
				onChangeText={(username) => {
					setUsername(username);
				}}
				label="Username"
				autoComplete={false}
			/>
			<TextInput
				value={password}
				style={styles.input}
				onChangeText={(password) => setPassword(password)}
				label="Password"
				secureTextEntry={passwordVisible}
				right={
					<TextInput.Icon
						name={passwordVisible ? "eye" : "eye-off"}
						onPress={() => setPasswordVisible(!passwordVisible)}
					/>
				}
				autoComplete={false}
			/>
			{isErrorOn && (
				<Text style={styles.errorMsg}>Username or password are incorrect</Text>
			)}

			<Pressable
				style={styles.register}
				onPress={() => {
					authService
						.login(username, password)
						.then(({ token }: any) => {
							onLoginCompleted({ username, token });
						})
						.catch((error) => {
							setIsErrorOn(true);
							console.error(error);
						});
				}}
			>
				<Text style={styles.signInText}>Sign In</Text>
			</Pressable>
			<View style={styles.signUpContainer}>
				<Text style={styles.signUpQuestion}>Don't have an account yet?</Text>
				<TouchableOpacity
					onPress={() => {
						redirectToRegister();
					}}
				>
					<Text style={styles.signUpText}>Sign Up</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		padding: 20,
	},
	input: {
		width: Dimensions.get("window").width - 50,
		margin: 8,
	},
	signUpText: {
		color: "black",
		textAlign: "center",
		marginBottom: 15,
		marginTop: 15,
		fontSize: 15,
		textDecorationLine: "underline",
	},
	errorMsg: {
		color: "red",
	},
	register: {
		backgroundColor: "black",
		width: Dimensions.get("window").width - 80,
		marginTop: 25,
	},
	signUpContainer: { display: "flex", flexDirection: "row" },
	signUpQuestion: {
		color: "black",
		textAlign: "center",
		marginBottom: 15,
		marginTop: 15,
		marginRight: 5,
		fontSize: 15,
	},
	signInText: {
		color: "white",
		textAlign: "center",
		marginBottom: 15,
		marginTop: 15,
		fontWeight: "bold",
		fontSize: 20,
	},
});
