import React, { useEffect } from "react";
import { useState } from "react";
import { Dimensions, Pressable } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { Modal, StyleSheet, Text, View } from "react-native";
import { useSignUpFields } from "../../hooks/useSignUpFields";
import { TextField } from "../../components/TextField/TextField";
import { TextInput } from "react-native-paper";
import { SIGN_UP_FIELDS } from "../../constants/SignUpFields";
import { getUser } from "../../utils/token";
import userService from "../../services/common-beckend/userService";
import DeleteAccountModal from "../../components/popups/DeleteAccountModal";
import { SafeAreaView } from "react-native-safe-area-context";

const AccountDetails = ({ close }: { close: Function }) => {
	const { firstName, lastName, dateOfBirth, setValue } = useSignUpFields();
	const [modalVisible, setModalVisible] = useState(true);
	const [deleteModalOpen, setDeleteModalOpen] = useState(false);

	useEffect(() => {
		getUser().then(({ username }) => {
			userService
				.find(username)
				.then(({ firstName, lastName, bDate }: any) => {
					setValue(SIGN_UP_FIELDS.FIRST_NAME, firstName);
					setValue(SIGN_UP_FIELDS.LAST_NAME, lastName);
					setValue(SIGN_UP_FIELDS.DATE_OF_BIRTH, bDate);
				})
				.catch((error) => {
					console.log({ error });
				});
		});
	}, []);
	const handleClose = () => {
		close();
		setModalVisible(false);
	};
	return (
		<>
			<Modal
				animationType="slide"
				visible={modalVisible}
				onRequestClose={() => {
					handleClose();
				}}
			>
				<SafeAreaView
					style={{
						height: Dimensions.get("window").height - 80,
					}}
				>
					<View style={styles.modalTitleContainer}>
						<Pressable onPress={() => handleClose()}>
							<Ionicons name="close" size={30} color="black"></Ionicons>
						</Pressable>
						<Text style={styles.modalTitle}>Account Details</Text>
					</View>
					<View
						style={{
							display: "flex",
							flexDirection: "column",
							justifyContent: "space-between",
							alignItems: "center",
							height: "100%",
						}}
					>
						<View>
							<TextField
								disabled
								value={firstName.value}
								style={styles.input}
								error={firstName.error}
								errorStyle={styles.helperText}
								errorMsg="First Name is invalid!"
								label="First Name"
								onChangeText={(firstName: any) => {
									setValue(SIGN_UP_FIELDS.FIRST_NAME, firstName);
								}}
							/>
							<TextField
								disabled
								value={lastName.value}
								style={styles.input}
								error={lastName.error}
								errorStyle={styles.helperText}
								errorMsg="Last Name is invalid!"
								label="Last Name"
								onChangeText={(lastName: any) => {
									setValue(SIGN_UP_FIELDS.LAST_NAME, lastName);
								}}
							/>
							<TextInput
								disabled
								value={dateOfBirth.value}
								style={styles.input}
								onChangeText={(dateOfBirth: any) => {
									setValue(SIGN_UP_FIELDS.DATE_OF_BIRTH, dateOfBirth);
								}}
								label="Date of Birth"
								autoComplete={false}
							/>
						</View>
						<Pressable
							style={[styles.applyButton, styles.button]}
							onPress={() => {
								setDeleteModalOpen(true);
								// getUser().then(({ username }) => {
								// 	userService.delete(username).then(() => {
								// 		handleClose();
								// 	});
								// });
							}}
						>
							<Text style={[styles.defaultText, { color: "white" }]}>
								Delete
							</Text>
						</Pressable>
					</View>
				</SafeAreaView>
			</Modal>
			<DeleteAccountModal
				open={deleteModalOpen}
				setModalVisible={setDeleteModalOpen}
			/>
		</>
	);
};

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		padding: 20,
	},
	input: {
		width: Dimensions.get("window").width - 50,
		margin: 8,
	},
	genderBoxContainer: {
		marginTop: 15,
		display: "flex",
		flexDirection: "row",
		width: Dimensions.get("window").width - 50,
		justifyContent: "space-around",
		// gap: 9,
	},
	genderBox: {
		borderColor: "grey",
		borderWidth: 1,
		borderRadius: 5,
		flex: 3,
		margin: "0px 5px",
		textAlign: "center",
	},
	chosenGender: {
		borderColor: "black",
		borderWidth: 1,
		borderRadius: 5,
		flex: 3,
		margin: "0px 5px",
		textAlign: "center",
	},
	genderText: {
		color: "grey",
		fontSize: 13,
		textAlign: "center",
		marginTop: 15,
		marginBottom: 15,
	},
	chosenGenderText: {
		color: "black",
		fontSize: 13,
		textAlign: "center",
		marginTop: 15,
		marginBottom: 15,
		fontWeight: "bold",
	},
	register: {
		backgroundColor: "black",
		width: Dimensions.get("window").width - 80,
		marginTop: 25,
	},
	signUpText: {
		color: "white",
		textAlign: "center",
		marginBottom: 15,
		marginTop: 15,
		fontWeight: "bold",
		fontSize: 20,
	},
	signInQuestion: {
		color: "black",
		textAlign: "center",
		marginBottom: 15,
		marginTop: 15,
		marginRight: 5,
		fontSize: 15,
	},
	signInText: {
		color: "black",
		textAlign: "center",
		marginBottom: 15,
		marginTop: 15,
		fontSize: 15,
		textDecorationLine: "underline",
	},
	modalTitle: {
		fontSize: 22,
		marginLeft: 10,
		marginStart: "15%",
		fontWeight: "bold",
	},
	mainTitle: {
		fontSize: 30,
		fontWeight: "bold",
		marginBottom: 20,
		textAlign: "center",
	},
	subTitle: {
		fontSize: 18,
		lineHeight: 24,
		marginBottom: 10,
		marginStart: 8,
		textAlign: "center",
		color: "grey",
	},
	signInContainer: { display: "flex", flexDirection: "row" },
	helperText: {
		textAlign: "left",
		width: Dimensions.get("window").width - 33,
		paddingLeft: "0px",
	},
	button: {
		color: "white",
		borderRadius: 56,
		textAlign: "center",
		marginTop: 20,
		display: "flex",
		justifyContent: "center",
		flexDirection: "column",
		width: "95%",
		margin: 10,
		height: 50,
		alignItems: "center",
	},
	applyButton: {
		backgroundColor: "rgb(0, 0, 0)",
	},
	defaultText: {
		fontSize: 17,
	},
	modalTitleContainer: {
		width: Dimensions.get("window").width - 30,
		flexDirection: "row",
		alignContent: "center",
		display: "flex",
		alignItems: "center",
		// justifyContent: "center",
		// marginLeft: 13,
		marginLeft: 20,
		marginBottom: 30,
		marginTop: 15,
		// borderColor
	},
});

export default AccountDetails;
