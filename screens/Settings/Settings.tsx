import React, { useEffect } from "react";
import { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import {
	Dimensions,
	Modal,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from "react-native";
import { Checkbox } from "react-native-paper";
import userService from "../../services/common-beckend/userService";
import { getUser } from "../../utils/token";
import { SafeAreaView } from "react-native-safe-area-context";
import { NunText } from "../../components/StyledText";

const SettingsScreen = ({ close }: { close: Function }) => {
	const [modalVisible, setModalVisible] = useState(true);

	const handleClose = () => {
		close();
		setModalVisible(false);
	};
	const [notificationPreferences, setNotificationPreferences] = useState({
		email: false,
		pushNotifications: false,
		sms: false,
	});

	useEffect(() => {
		getUser().then(({ username }) => {
			userService.find(username).then((user: any) => {
				setNotificationPreferences({
					email: user.emailEnabled,
					pushNotifications: user.pushNotificationEnabled,
					sms: false,
				});
			});
		});
	}, []);

	return (
		<Modal
			animationType="slide"
			visible={modalVisible}
			onRequestClose={() => {
				handleClose();
			}}
		>
			<SafeAreaView>
				<View
					style={{
						display: "flex",
						flexDirection: "column",
						justifyContent: "space-between",
						height: Dimensions.get("window").height - 10,
					}}
				>
					<View>
						<View style={styles.container}>
							{/* <FontAwesome size={26} name="user" /> */}
							<TouchableOpacity
								style={{ marginTop: 4 }}
								onPress={() => handleClose()}
							>
								<Ionicons name="close" size={30} color="black"></Ionicons>
							</TouchableOpacity>

							<Text style={styles.title}>Settings</Text>
						</View>
						<View
							style={{
								display: "flex",
								flexDirection: "column",
								marginTop: 42,
								marginLeft: 13,
							}}
						>
							<View style={{ marginBottom: 15 }}>
								<Text
									style={{ marginBottom: 8, fontSize: 18, fontWeight: "bold" }}
								>
									Notification Preferences
								</Text>
								<Text>Get Notified By</Text>
							</View>
							<View
								style={{
									display: "flex",
									flexDirection: "row",
									alignItems: "center",
									justifyContent: "space-between",
									marginRight: 10,
									borderStyle: "solid",
									borderBottomColor: "gray",
									borderBottomWidth: 1,
									padding: 5,
								}}
							>
								<Text style={{ fontSize: 17 }}>Email</Text>
								<Checkbox
									color="#55aaff"
									status={
										notificationPreferences.email ? "checked" : "unchecked"
									}
									onPress={() => {
										setNotificationPreferences((prev) => ({
											...prev,
											email: !prev.email,
										}));
									}}
								/>
							</View>
							<View
								style={{
									display: "flex",
									flexDirection: "row",
									alignItems: "center",
									justifyContent: "space-between",
									marginRight: 10,
									borderStyle: "solid",
									borderBottomColor: "gray",
									borderBottomWidth: 1,
									padding: 5,
								}}
							>
								<Text style={{ fontSize: 17 }}>Push notifications</Text>
								<Checkbox
									color="#55aaff"
									status={
										notificationPreferences.pushNotifications
											? "checked"
											: "unchecked"
									}
									onPress={() => {
										setNotificationPreferences((prev) => ({
											...prev,
											pushNotifications: !prev.pushNotifications,
										}));
									}}
								/>
							</View>
							<View
								style={{
									display: "flex",
									flexDirection: "row",
									alignItems: "center",
									justifyContent: "space-between",
									marginRight: 10,
									borderStyle: "solid",
									borderBottomColor: "gray",
									borderBottomWidth: 1,
									padding: 5,
								}}
							>
								<Text style={{ fontSize: 17 }}>SMS</Text>
								<Checkbox
									color="#55aaff"
									status={notificationPreferences.sms ? "checked" : "unchecked"}
									onPress={() => {
										setNotificationPreferences((prev) => ({
											...prev,
											sms: !prev.sms,
										}));
									}}
								/>
							</View>
						</View>
					</View>

					<TouchableOpacity
						style={[styles.applyButton, styles.button]}
						onPress={() => {
							getUser().then(({ username }) => {
								console.log(username);
								console.log(notificationPreferences.email);
								console.log(notificationPreferences.pushNotifications);
								userService.notifications(
									username,
									notificationPreferences.email,
									notificationPreferences.pushNotifications
								);
								handleClose();
							});
						}}
					>
						<Text style={[styles.defaultText, { color: "white" }]}>
							Save Preferences
						</Text>
					</TouchableOpacity>
				</View>
			</SafeAreaView>
		</Modal>
	);
};

const styles = StyleSheet.create({
	modalContainer: {
		justifyContent: "center",
		// margin: 15,
		// padding: 8,
	},
	section: {
		flexDirection: "row",
		alignItems: "center",
	},
	checkbox: {
		margin: 8,
	},
	modalTitle: {
		marginBottom: 20,
		marginTop: 17,
		textAlign: "center",
		fontSize: 24,
	},
	modalTitleContainer: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
	},
	openModalButton: {
		borderWidth: 1,
		borderColor: "rgba(0,0,0,0.2)",
		alignItems: "center",
		width: 100,
		position: "absolute",
		height: 38,
		borderRadius: 100,
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
	},
	openModalbuttonText: {
		fontSize: 17,
		paddingRight: 7,
	},
	line: {
		borderBottomColor: "rgb(229, 229, 229)",
		borderBottomWidth: 1,
		marginTop: 15,
	},
	bottomButtons: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-evenly",
	},
	button: {
		color: "white",
		borderRadius: 56,
		textAlign: "center",
		marginTop: 20,
		display: "flex",
		justifyContent: "center",
		flexDirection: "column",
		// width: "95%",
		margin: 10,
		height: 50,
		alignItems: "center",
	},
	applyButton: {
		backgroundColor: "black",
		// borderColor: "black",
		// borderWidth: 2,
		// borderStyle: "solid",
	},
	clearButton: {
		borderColor: "rgba(0,0,0,0.2)",
		borderWidth: 1,
	},
	defaultText: {
		fontSize: 17,
	},
	title: {
		fontSize: 22,
		marginLeft: 10,
		marginStart: "30%",
		fontWeight: "bold",
	},
	container: {
		width: Dimensions.get("window").width - 30,
		flexDirection: "row",
		alignContent: "center",
		display: "flex",
		alignItems: "center",
		// justifyContent: "center",
		// marginLeft: 13,
		margin: 8,
		// borderColor
	},
});

export default SettingsScreen;
