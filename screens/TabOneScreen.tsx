import { Feather } from "@expo/vector-icons";
import React, { useCallback, useContext, useState } from "react";
import { Pressable, StyleSheet, Linking, Alert } from "react-native";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import { UserContext } from "../App";
import { FontAwesome } from "@expo/vector-icons";
import AccountDetails from "./AccountDetails/AccountDetails";
import SettingsScreen from "./Settings/Settings";

export default function TabOneScreen({
	navigation,
}: RootTabScreenProps<"TabThree">) {
	const [accountDetailsScreen, setAccountDetailsScreen] = useState(false);
	const [settingsScreen, setSettingsScreen] = useState(false);

	const { logout } = useContext(UserContext);

	const aboutUrl = "https://gitlab.com/c6912/clubme-privacy";
	const handleAboutPress = useCallback(async () => {
		// Checking if the link is supported for links with custom URL scheme.
		const supported = await Linking.canOpenURL(aboutUrl);

		if (supported) {
			// Opening the link with some app, if the URL scheme is "http" the web link should be opened
			// by some browser in the mobile
			await Linking.openURL(aboutUrl);
		} else {
			Alert.alert(`Don't know how to open this URL: ${aboutUrl}`);
		}
	}, []);

	const contactUrl = "https://wa.me/+972544568541";
	const handleContactPress = useCallback(async () => {
		// Checking if the link is supported for links with custom URL scheme.
		const supported = await Linking.canOpenURL(contactUrl);

		if (supported) {
			// Opening the link with some app, if the URL scheme is "http" the web link should be opened
			// by some browser in the mobile
			await Linking.openURL(contactUrl);
		} else {
			Alert.alert(`Don't know how to open this URL: ${contactUrl}`);
		}
	}, []);

	return (
		<View style={styles.container}>
			{accountDetailsScreen && (
				<AccountDetails
					close={() => {
						setAccountDetailsScreen(false);
					}}
				/>
			)}
			<Pressable
				style={styles.optionContainer}
				onPress={() => {
					setAccountDetailsScreen(true);
				}}
			>
				<View style={styles.option}>
					<Feather name="user" size={25} style={styles.optionIcon} />
					<Text style={styles.optionText}>Account Details</Text>
				</View>
				<FontAwesome
					size={18}
					name="chevron-right"
					onPress={() => {
						console.log("bell");
					}}
				/>
			</Pressable>
			{settingsScreen && (
				<SettingsScreen
					close={() => {
						setSettingsScreen(false);
					}}
				/>
			)}
			<Pressable
				style={styles.optionContainer}
				onPress={() => {
					setSettingsScreen(true);
				}}
			>
				<View style={styles.option}>
					<Feather name="settings" size={25} style={styles.optionIcon} />
					<Text style={styles.optionText}>Settings</Text>
				</View>
				<FontAwesome
					size={18}
					name="chevron-right"
					onPress={() => {
						console.log("bell");
					}}
				/>
			</Pressable>
			<Pressable
				style={styles.optionContainer}
				onPress={() => {
					handleContactPress();
				}}
			>
				<View style={styles.option}>
					<Feather name="phone" size={25} style={styles.optionIcon} />
					<Text style={styles.optionText}>Contact</Text>
				</View>
				<FontAwesome
					size={18}
					name="chevron-right"
					onPress={() => {
						console.log("bell");
					}}
				/>
			</Pressable>
			<Pressable
				style={styles.optionContainer}
				onPress={() => {
					handleAboutPress();
				}}
			>
				<View style={styles.option}>
					<Feather name="user-check" size={25} style={styles.optionIcon} />
					<Text style={styles.optionText}>About</Text>
				</View>
				<FontAwesome
					size={18}
					name="chevron-right"
					onPress={() => {
						console.log("bell");
					}}
				/>
			</Pressable>
			<Pressable
				style={styles.optionContainer}
				onPress={() => {
					logout();
				}}
			>
				<View style={styles.option}>
					<Feather name="log-out" size={25} style={styles.optionIcon} />
					<Text style={styles.optionText}>Log out</Text>
				</View>
				{/* <FontAwesome
					size={18}
					name="chevron-right"
					onPress={() => {
						console.log("bell");
					}}
				/> */}
			</Pressable>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		display: "flex",
		alignItems: "center",
		paddingTop: 20,
	},
	optionContainer: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
		minHeight: 50,
		borderBottomWidth: 1,
		borderBottomColor: "#ccc",
	},
	option: {
		display: "flex",
		flexDirection: "row",
		// width: "100%",
		alignItems: "center",
	},
	optionText: {
		fontSize: 17,
	},
	optionIcon: {
		paddingRight: 20,
		marginLeft: 20,
	},
});
