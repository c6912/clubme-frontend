import { Searchbar } from "react-native-paper";
import FilterModal from "../../components/popups/filterModel";
import { Card } from "../../components/MenuCard/MenuCard";
import AddStoreCreditsModal from "../../components/popups/addModals/addStoreCredits";
import React, { useEffect, useState } from "react";
import {
	Text,
	FlatList,
	TouchableOpacity,
	Dimensions,
	StyleSheet,
	View,
} from "react-native";
import { getUser } from "../../utils/token";

import storeCreditUserService from "../../services/storeCreditUserService";

import { CardDetailsModal } from "../../components/popups/CardDetailsModal";
import { CardTypes, Item } from "../../components/PopupCard/PopupCard.types";
import { useStoreCredits } from "../../context/StoreCredits/storeCredits-context";
import { CARD_BELONGING_TYPES, CATEGORIES } from "../../constants/Categories";
import { StoreCreditsDetailsModal } from "../../components/popups/storeCreditsDetailsModal";

export const StoreCredits = () => {
	const { storeCredits, setStoreCredits } = useStoreCredits();
	const [filteredStoreCredits, setFilteredStoreCredits] =
		useState(storeCredits);

	const [cardModalOpen, setCardModalOpen] = useState(false);

	const [searchQuery, setSearchQuery] = React.useState("");

	const [activeCardIndex, setActiveCardIndex] = useState(null);

	const handleClose = () => {
		setActiveCardIndex(null);
		setCardModalOpen(false);
	};

	useEffect(() => {
		handleFilterStoreCredits(new Array(CATEGORIES.length).fill(true), [
			true,
			false,
			false,
		]);
	}, [storeCredits]);

	const handleFilterStoreCredits = (
		checkedState: boolean[],
		checkedCardType: boolean[]
	) => {
		const filteredByCategories = storeCredits.filter((membership) => {
			const categories = membership?.["categories"] as Array<any>;
			let isOk = false;
			isOk = categories?.some((category) => checkedState[category?.["id"]]);
			if (isOk) return isOk;
			categories?.forEach((element, index) => {
				if (element && checkedState[index]) {
					isOk = true;
					return;
				}
			});
			return isOk;
		});

		const filteredByBelonging = filteredByCategories.filter((membership) => {
			if (
				membership?.["shared"] &&
				checkedCardType[CARD_BELONGING_TYPES.SHARED.id]
			)
				return true;
			if (
				!membership?.["shared"] &&
				checkedCardType[CARD_BELONGING_TYPES.MINE.id]
			)
				return true;
			return false;
		});

		setFilteredStoreCredits(filteredByBelonging);
	};

	const onChangeSearch = (query: string) => {
		setSearchQuery(query);

		const filteredStoreCredits = storeCredits.filter(
			(giftCard: any) =>
				giftCard.name.includes(query) || giftCard.store.includes(query)
		);
		setFilteredStoreCredits(filteredStoreCredits);
	};

	const renderItem = ({ item, index }: { item: Item; index: number }) => {
		return (
			<>
				<TouchableOpacity
					onPress={() => {
						console.log({ index });
						setCardModalOpen(true);
						setActiveCardIndex(index);
					}}
				>
					<Card
						type={CardTypes.StoreCredit}
						title={item.name}
						shared={item.shared}
						store={item.store}
						isFavorite={item.isFavorite}
						onFavoriteClick={() => {
							setFilteredStoreCredits((prevStoreCredits) => {
								const newStoreCredits = [...prevStoreCredits];
								newStoreCredits[index].isFavorite =
									!newStoreCredits[index].isFavorite;
								return newStoreCredits;
							});
							setStoreCredits((prevStoreCredits) =>
								prevStoreCredits.map((card) =>
									card.id === item.id
										? { ...card, isFavorite: item.isFavorite }
										: card
								)
							);

							getUser().then(({ username }) => {
								storeCreditUserService.setFavorite(
									username,
									item.id,
									filteredStoreCredits[index].isFavorite
								);
							});
						}}
					/>
				</TouchableOpacity>
			</>
		);
	};

	return (
		<>
			<View
				style={{
					width: Dimensions.get("window").width - 30,
					display: "flex",
					flexDirection: "row",
					justifyContent: "space-between",
					alignItems: "center",
					backgroundColor: "white",
				}}
			>
				<Searchbar
					style={styles.input}
					placeholder="Search"
					onChangeText={onChangeSearch}
					value={searchQuery}
				/>
				<View style={{ flex: "0 0 100px", marginRight: 12 }}>
					<FilterModal handleFilterCards={handleFilterStoreCredits} />
				</View>
			</View>
			{filteredStoreCredits.length ? (
				<View
					style={{
						overflowY: "auto",
						height: 500,
					}}
				>
					<FlatList
						numColumns={2}
						data={filteredStoreCredits}
						extraData={filteredStoreCredits}
						renderItem={renderItem}
						keyExtractor={(item, index) => item.id + index}
					/>
				</View>
			) : (
				<Text>No Store credits found</Text>
			)}
			<View
				style={{
					position: "absolute",
					top: Dimensions.get("window").height - 100,
				}}
			>
				<AddStoreCreditsModal />
			</View>
			{cardModalOpen && (
				<StoreCreditsDetailsModal
					{...filteredStoreCredits[activeCardIndex]}
					type={CardTypes.StoreCredit}
					open={cardModalOpen}
					onClose={() => handleClose()}
				/>
			)}
		</>
	);
};

const styles = StyleSheet.create({
	item: {
		display: "flex",
		justifyContent: "space-around",
		backgroundColor: "#dddddd",
		padding: 5,
		width: (Dimensions.get("window").width - 50) / 2,
		height: 100,
		marginVertical: 3,
		marginHorizontal: 3,
		borderRadius: 10,
	},
	title: {
		fontSize: 20,
		color: "#586589",
	},
	dropdown: {
		// marginLeft: "15%"
	},
	input: {
		height: 40,
		margin: 12,
		borderWidth: 1,
		borderColor: "rgba(0, 0, 0, 0.2)",
		borderRadius: 100,
		padding: 10,
		width: 200,
		// shadowColor: "none",
	},
});
