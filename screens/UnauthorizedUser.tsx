import { UserContext } from "../App";
import React, { createElement, useContext, useState } from "react";
import { StyleSheet } from "react-native";

import { View } from "../components/Themed";
import LoginScreen from "./LoginScreen";
import RegisterScreen from "./SignUpScreen/SignUpScreen";

enum PAGES_ID {
	LOGIN,
	REGISTER,
}

export default function UnauthorizedUser() {
	const { login } = useContext(UserContext);
	const [currentPageId, setCurrentPageId] = useState<number>(0);

	const redirectToLogin = () => {
		setCurrentPageId(PAGES_ID.LOGIN);
	};
	const redirectToRegister = () => {
		setCurrentPageId(PAGES_ID.REGISTER);
	};
	const onRegisterCompleted = (user: any) => {
		login(user);
	};

	const onLoginCompleted = (user: any) => {
		login(user);
	};

	const Pages: any = {
		[PAGES_ID.LOGIN]: {
			element: LoginScreen,
			props: { redirectToRegister, onLoginCompleted },
			children: null,
		},
		[PAGES_ID.REGISTER]: {
			element: RegisterScreen,
			props: { redirectToLogin, onRegisterCompleted },
			children: null,
		},
	};

	return (
		<View style={styles.container}>
			{createElement(
				Pages[currentPageId].element,
				Pages[currentPageId].props,
				Pages[currentPageId].children
			)}
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		padding: 20,
	},
	input: {
		width: 200,
		margin: 8,
	},
	signUpText: {
		marginTop: 15,
		marginBottom: 15,
	},
});
