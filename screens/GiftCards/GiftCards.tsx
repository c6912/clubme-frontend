import { useNavigation } from "@react-navigation/native";
import { Searchbar } from "react-native-paper";
import { GiftCard } from "../../components/GiftCard/GiftCard";
import FilterModal from "../../components/popups/filterModel";
import type { GiftCardProps } from "../../components/GiftCard/GiftCard.types";
import { Card } from "../../components/MenuCard/MenuCard";
import AddGiftCardModal from "../../components/popups/addModals/addGiftCardModal";
import React, { useEffect, useState } from "react";
import {
	Text,
	FlatList,
	TouchableOpacity,
	Dimensions,
	StyleSheet,
	View,
} from "react-native";
import { getUser } from "../../utils/token";
import giftcardsUserService from "../../services/giftcardsUserService";
import { useGiftCards } from "../../context/giftCards/giftcards-context";
import { CardDetailsModal } from "../../components/popups/CardDetailsModal";
import { CardTypes, Item } from "../../components/PopupCard/PopupCard.types";
import { CARD_BELONGING_TYPES, CATEGORIES } from "../../constants/Categories";
import webScannerService from "../../services/webScannerService";
import { GiftCardDetailsModal } from "../../components/popups/giftCardDetailsModal";
import { SafeAreaView } from "react-native-safe-area-context";

export const GiftCards = () => {
	const { giftCards, setGiftCards } = useGiftCards();
	const [filteredGiftCards, setFilteredGiftCards] = useState(giftCards);
	const [filteredByCategories, setFilteredByCategories] = useState([]);
	const [cardModalOpen, setCardModalOpen] = useState(false);
	const [searchQuery, setSearchQuery] = React.useState("");
	const [activeCardIndex, setActiveCardIndex] = useState(null);

	useEffect(() => {
		handleFilterGiftCards(new Array(CATEGORIES.length).fill(true), [
			true,
			false,
			false,
		]);
	}, [giftCards]);

	const handleClose = () => {
		setActiveCardIndex(null);
		setCardModalOpen(false);
	};

	const handleFilterGiftCards = async (
		checkedState: boolean[],
		checkedCardType: boolean[]
	) => {
		const filteredByCategories = giftCards.filter((membership) => {
			const categories = membership?.["categories"] as Array<any>;
			let isOk = false;
			isOk = categories?.some((category) => checkedState[category?.["id"]]);
			if (isOk) return isOk;
			categories.forEach((element, index) => {
				if (element && checkedState[index]) {
					isOk = true;
					return;
				}
			});
			return isOk;
		});
		const filteredByBelonging = filteredByCategories.filter((giftcard) => {
			if (
				giftcard?.["shared"] &&
				checkedCardType[CARD_BELONGING_TYPES.SHARED.id]
			)
				return true;
			if (
				!giftcard?.["shared"] &&
				checkedCardType[CARD_BELONGING_TYPES.MINE.id]
			)
				return true;
			return false;
		});
		let modify: Array<any> = [];
		if (checkedCardType[CARD_BELONGING_TYPES.EXTERNAL.id]) {
			const externalGiftCard: Array<any> =
				await webScannerService.getHeverCoupons();

			modify = externalGiftCard.map((giftcard) => ({
				...giftcard,
				type: CardTypes.GiftCard,
				isSharedCard: false,
				external: true,
				name: giftcard.link,
				store: giftcard.description,
			}));
		}

		setFilteredByCategories([...filteredByBelonging, ...modify]);
		setFilteredGiftCards([...filteredByBelonging, ...modify]);
	};

	const onChangeSearch = (query: string) => {
		setSearchQuery(query);

		const filtered = filteredByCategories.filter(
			(giftCard: any) =>
				giftCard.name.includes(query) || giftCard.store.includes(query)
		);
		console.log({ filtered });
		setFilteredGiftCards(filtered);
	};

	const renderItem = ({ item, index }: { item: Item; index: number }) => {
		return (
			<>
				<SafeAreaView>
					<TouchableOpacity
						onPress={() => {
							console.log({ index });
							setCardModalOpen(true);
							setActiveCardIndex(index);
						}}
					>
						<Card
							key={index}
							type={CardTypes.GiftCard}
							title={item.name}
							store={item.store}
							isFavorite={item.isFavorite}
							shared={item.shared}
							onFavoriteClick={() => {
								setFilteredGiftCards((prevGiftCards) => {
									const newGiftCards = [...prevGiftCards];
									newGiftCards[index].isFavorite =
										!newGiftCards[index].isFavorite;
									return newGiftCards;
								});
								setGiftCards((prevGiftCards) =>
									prevGiftCards.map((card) =>
										card.id === item.id
											? { ...card, isFavorite: item.isFavorite }
											: card
									)
								);
								getUser().then(({ username }) => {
									giftcardsUserService.setFavorite(
										username,
										item.id,
										filteredGiftCards[index].isFavorite
									);
								});
							}}
						/>
					</TouchableOpacity>
				</SafeAreaView>
			</>
		);
	};

	return (
		<>
			<View
				style={{
					width: Dimensions.get("window").width - 30,
					display: "flex",
					flexDirection: "row",
					justifyContent: "space-between",
					alignItems: "center",
					backgroundColor: "white",
				}}
			>
				<Searchbar
					style={styles.input}
					placeholder="Search"
					onChangeText={onChangeSearch}
					value={searchQuery}
				/>
				<View style={{ flex: "0 0 100px", marginRight: 12 }}>
					<FilterModal handleFilterCards={handleFilterGiftCards} />
				</View>
			</View>
			{filteredGiftCards.length ? (
				<View
					style={{
						overflowY: "auto",
						height: 500,
					}}
				>
					<FlatList
						numColumns={2}
						data={filteredGiftCards}
						extraData={filteredGiftCards}
						renderItem={renderItem}
						keyExtractor={(item, index) => item.id + index}
					/>
				</View>
			) : (
				<Text>No Gift cards found</Text>
			)}
			<View
				style={{
					position: "absolute",
					top: Dimensions.get("window").height - 100,
				}}
			>
				<AddGiftCardModal />
			</View>
			{cardModalOpen && (
				<GiftCardDetailsModal
					{...filteredGiftCards[activeCardIndex]}
					isSharedCard={filteredGiftCards[activeCardIndex]?.["shared"]}
					type={CardTypes.GiftCard}
					open={cardModalOpen}
					onClose={() => handleClose()}
				/>
			)}
		</>
	);
};

const styles = StyleSheet.create({
	item: {
		display: "flex",
		justifyContent: "space-around",
		backgroundColor: "#dddddd",
		padding: 5,
		width: (Dimensions.get("window").width - 50) / 2,
		height: 100,
		marginVertical: 3,
		marginHorizontal: 3,
		borderRadius: 10,
	},
	title: {
		fontSize: 20,
		color: "#586589",
	},
	dropdown: {
		// marginLeft: "15%"
	},
	input: {
		height: 40,
		margin: 12,
		borderWidth: 1,
		borderColor: "rgba(0, 0, 0, 0.2)",
		borderRadius: 100,
		padding: 10,
		width: 200,
		// shadowColor: "none",
	},
});
