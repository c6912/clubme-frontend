import React, { useState } from "react";
import { StatusBar, StyleSheet, Dimensions, Text } from "react-native";
import { View } from "../components/Themed";

import { CardsTabs } from "../components/CardsTabs/CardsTabs";
import { cardsTabs } from "../constants/CardsTabs";
import { GiftCards } from "./GiftCards/GiftCards";
import { StoreCredits } from "./StoreCredits/StoreCredits";
import { MembershipCards } from "./Membership/Membership";

export default function TabTwoScreen() {
	const [activeTab, setActiveTab] = useState(0);

	return (
		<View style={styles.container}>
			<CardsTabs
				tabs={cardsTabs}
				onTabChanged={(tab: any) => {
					setActiveTab(tab);
				}}
			/>

			<View
				style={{
					margin: "15px 0px 0px 0px",
					opacity: 0.9,
				}}
			>
				{activeTab === 0 ? (
					<MembershipCards />
				) : activeTab === 1 ? (
					<GiftCards />
				) : (
					<StoreCredits />
				)}
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		display: "flex",
		alignItems: "center",
		marginTop: StatusBar.currentHeight || 0,
	},
	item: {
		display: "flex",
		justifyContent: "space-around",
		backgroundColor: "#dddddd",
		padding: 5,
		width: Dimensions.get("window").width / 2,
		height: 100,
		marginVertical: 3,
		marginHorizontal: 3,
		borderRadius: 10,
	},
	title: {
		fontSize: 20,
		color: "#586589",
	},
	dropdown: {},
	input: {
		height: 40,
		margin: 12,
		borderWidth: 1,
		borderColor: "rgba(0, 0, 0, 0.2)",
		borderRadius: 100,
		padding: 10,
		width: 200,
	},
});
