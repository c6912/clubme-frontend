import React, { useEffect, useState } from "react";
import {
	Dimensions,
	FlatList,
	ScrollView,
	StatusBar,
	StyleSheet,
	Text,
	TouchableOpacity,
} from "react-native";
import { HomeScrollView } from "../components/HomeScrollView/HomeScrollView";
import { Card } from "../components/MenuCard/MenuCard";
import { Item } from "../components/PopupCard/PopupCard.types";
import { CardDetailsModal } from "../components/popups/CardDetailsModal";
import { RecommendationCard } from "../components/Recommendation/Recommendation";
import {
	adaptRecommendationToCard,
	Recommendation,
} from "../components/Recommendation/Recommendation.type";
import { View } from "../components/Themed";
import { useGiftCards } from "../context/giftCards/giftcards-context";
import { useMembershipCards } from "../context/MembershipCards/membershipcards-context";
import { useStoreCredits } from "../context/StoreCredits/storeCredits-context";
import shareService from "../services/common-beckend/shareService";
import userService from "../services/common-beckend/userService";
import { RootTabScreenProps } from "../types";
import { getUser } from "../utils/token";

const { width } = Dimensions.get("window");

export default function TabThreeScreen({
	navigation,
}: RootTabScreenProps<"TabOne">) {
	const [allSharedCards, setAllSharedCards] = useState<any>([]);
	const [activeCard, setActiveCard] = useState<Item>(null);
	const [allFavoritesCards, setAllFavoritesCards] = useState<any>([]);
	const [allRecommendations, setAllRecommendations] = useState<
		Recommendation[]
	>([]);

	const { giftCards } = useGiftCards();
	const { membershipCards } = useMembershipCards();
	const { storeCredits } = useStoreCredits();

	useEffect(() => {
		getUser().then(({ username }) => {
			if (username) {
				shareService.getMySharedCards(username).then((sharedCards: any) => {
					setAllSharedCards([...sharedCards]);
				});

				userService.recommendations(username).then((recommendations: any) => {
					setAllRecommendations([...recommendations]);
				});
			}
		});
	}, []);

	useEffect(() => {
		setAllFavoritesCards((prevFavorites) => [
			...giftCards.filter(({ isFavorite }) => isFavorite),
			...membershipCards.filter(({ isFavorite }) => isFavorite),
			...storeCredits.filter(({ isFavorite }) => isFavorite),
		]);
	}, [giftCards, membershipCards, storeCredits]);

	const renderSharedCard = ({ item }: { item: Item }) => (
		<TouchableOpacity
			onPress={() => {
				setActiveCard(item);
			}}
		>
			<Card
				type={item.type}
				title={item.name}
				store={item.store}
				shared={true}
				shouldShowFavorite={false}
			/>
		</TouchableOpacity>
	);

	const renderRecommendationCard = ({ item }: { item: Recommendation }) => (
		<TouchableOpacity
			onPress={() => {
				setActiveCard(adaptRecommendationToCard(item) as any);
			}}
		>
			<RecommendationCard cardName={item.cardName} cardPrice={item.cardPrice} />
		</TouchableOpacity>
	);

	const renderFavoriteCard = ({ item }: { item: Item }) => (
		<TouchableOpacity
			onPress={() => {
				setActiveCard(item);
			}}
		>
			<Card
				type={item.type}
				title={item.name}
				store={item.store}
				shared={item.shared}
				isFavorite={item.isFavorite}
				shouldShowFavorite={false}
				onFavoriteClick={() => {}}
			/>
		</TouchableOpacity>
	);

	return (
		<View style={styles.container}>
			<ScrollView>
				<HomeScrollView title="Favorites" snapToInterval={width - 220}>
					<View>
						{allFavoritesCards.length ? (
							<FlatList
								horizontal={true}
								data={allFavoritesCards}
								extraData={allFavoritesCards}
								renderItem={renderFavoriteCard}
								keyExtractor={(item, index) => item.id + index}
							/>
						) : (
							<Text
								style={{
									fontSize: 16,
									marginTop: 10,
								}}
							>
								No Favorites Cards
							</Text>
						)}
					</View>
				</HomeScrollView>

				<HomeScrollView title="Recommendations" snapToInterval={width - 140}>
					<View>
						{allRecommendations.length ? (
							<FlatList
								horizontal={true}
								data={allRecommendations}
								extraData={allRecommendations}
								renderItem={renderRecommendationCard}
								keyExtractor={(item, index) => item.id + index}
							/>
						) : (
							<Text
								style={{
									fontSize: 16,
									marginTop: 10,
								}}
							>
								No recommendations
							</Text>
						)}
					</View>
				</HomeScrollView>

				<HomeScrollView title="Shared cards" snapToInterval={width - 140}>
					<View>
						{allSharedCards.length ? (
							<FlatList
								horizontal={true}
								data={allSharedCards}
								extraData={allSharedCards}
								renderItem={renderSharedCard}
								keyExtractor={(item, index) => item.id + index}
							/>
						) : (
							<Text
								style={{
									fontSize: 16,
									marginTop: 10,
								}}
							>
								No shared cards
							</Text>
						)}
					</View>
				</HomeScrollView>
			</ScrollView>
			{activeCard != null && (
				<CardDetailsModal
					{...activeCard}
					open={activeCard != null}
					onClose={() => setActiveCard(null)}
				/>
			)}
		</View>
	);
}

const styles = StyleSheet.create({
	title: {
		fontSize: 20,
		fontWeight: "bold",
	},
	separator: {
		marginVertical: 30,
		height: 1,
		width: "80%",
	},
	container: {
		flex: 1,
		paddingTop: StatusBar.currentHeight,
	},
	view: {
		backgroundColor: "rgb(221, 221, 221)",
		width: width - 260,
		margin: 4,
		height: 90,
		borderRadius: 10,
		display: "flex",
		justifyContent: "space-around",
	},
});
