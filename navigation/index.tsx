/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { FontAwesome } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
	DarkTheme,
	DefaultTheme,
	NavigationContainer,
} from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as React from "react";
import { ColorSchemeName } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { HomeHeader } from "../components/HomeHeader/HomeHeader";
import PopupCard from "../components/PopupCard/PopupCard";
import { CardTypes } from "../components/PopupCard/PopupCard.types";
import { UserHeader } from "../components/UserHeader/UserHeader";
import Colors from "../constants/Colors";
import { useGiftCards } from "../context/giftCards/giftcards-context";
import { useMembershipCards } from "../context/MembershipCards/membershipcards-context";
import { useStoreCredits } from "../context/StoreCredits/storeCredits-context";
import useColorScheme from "../hooks/useColorScheme";
import ModalScreen from "../screens/ModalScreen";
import NotFoundScreen from "../screens/NotFoundScreen";
import TabOneScreen from "../screens/TabOneScreen";
import TabThreeScreen from "../screens/TabThreeScreen";
import TabTwoScreen from "../screens/TabTwoScreen";
import cardService from "../services/cardService";
import {
	RootStackParamList,
	RootTabParamList,
	RootTabScreenProps,
} from "../types";
import { getUser } from "../utils/token";
import LinkingConfiguration from "./LinkingConfiguration";

export default function Navigation({
	colorScheme,
}: {
	colorScheme: ColorSchemeName;
}) {
	const { setGiftCards } = useGiftCards();
	const { setMembershipCards } = useMembershipCards();
	const { setStoreCredits } = useStoreCredits();

	React.useEffect(() => {
		getUser().then(({ username }) => {
			if (username) {
				cardService.getCards(username).then((user: any) => {
					console.log({ user });
					const userId = user?.id;
					setGiftCards(
						user.giftcards.map((card) => ({
							...card,
							isFavorite: card["GiftCardsUsers"].isFavorite,
							type: CardTypes.GiftCard,
							shared: card.mainUserId !== userId,
						}))
					);
					setMembershipCards(
						user.MembershipCards.map((card) => ({
							...card,
							isFavorite: card["MembershipCardsUsers"].isFavorite,
							type: CardTypes.MembershipCard,
							shared: card.mainUserId !== userId,
						}))
					);
					setStoreCredits(
						user.StoreCredits.map((card) => ({
							...card,
							isFavorite: card["StoreCreditsUser"].isFavorite,
							type: CardTypes.StoreCredit,
							shared: card.mainUserId !== userId,
						}))
					);
				});
			}
		});
	}, []);

	return (
		<NavigationContainer
			linking={LinkingConfiguration}
			theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
		>
			<SafeAreaView style={{ flex: 1 }}>
				<RootNavigator />
			</SafeAreaView>
		</NavigationContainer>
	);
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
	return (
		<Stack.Navigator>
			<Stack.Screen
				name="Root"
				component={BottomTabNavigator}
				options={{ headerShown: false }}
			/>
			<Stack.Screen
				name="NotFound"
				component={NotFoundScreen}
				options={{ title: "Oops!" }}
			/>
		</Stack.Navigator>
	);
}

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator() {
	const colorScheme = useColorScheme();
	const [activePageIndex, setActivePageIndex] = React.useState(0);

	const onIconClick = (index) => {
		setActivePageIndex(index);
	};

	return (
		<BottomTab.Navigator
			initialRouteName="TabThree"
			screenOptions={{
				tabBarActiveTintColor: Colors[colorScheme].tint,
			}}
		>
			<BottomTab.Screen
				name="TabOne"
				component={TabOneScreen}
				options={({ navigation }: RootTabScreenProps<"TabOne">) => ({
					title: "My user",
					tabBarActiveTintColor: "rgb(85, 170, 255)",
					tabBarInactiveTintColor: "#586589",
					tabBarIcon: ({ color }) => (
						<TabBarIcon
							name="user"
							color={color}
							onClick={() => {
								onIconClick(2);
							}}
						/>
					),
					headerTitle: (props) => <UserHeader />,
				})}
			/>
			<BottomTab.Screen
				name="TabTwo"
				component={TabTwoScreen}
				options={{
					title: "Cards",
					headerShown: false,
					tabBarActiveTintColor: "rgb(85, 170, 255)",
					tabBarInactiveTintColor: "#586589",
					tabBarIcon: ({ color }) => (
						<TabBarIcon
							name="credit-card"
							color={color}
							onClick={() => {
								onIconClick(1);
							}}
						/>
					),
				}}
			/>

			<BottomTab.Screen
				name="TabThree"
				component={TabThreeScreen}
				options={{
					title: "Home",
					tabBarActiveTintColor: "rgb(85, 170, 255)",
					tabBarInactiveTintColor: "#586589",
					tabBarIcon: ({ color }) => (
						<TabBarIcon
							name="book"
							color={color}
							onClick={() => {
								// onIconClick(0);
							}}
						/>
					),
					headerTitle: (props) => <HomeHeader />,
				}}
			/>
		</BottomTab.Navigator>
	);
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
function TabBarIcon(props: {
	name: React.ComponentProps<typeof FontAwesome>["name"];
	color: string;
	onClick: Function;
}) {
	return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
}
