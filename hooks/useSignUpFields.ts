import { useState } from "react";
// Constant
import { GENDER } from "../constants/Gender";
import { SIGN_UP_FIELDS } from "../constants/SignUpFields";

export const useSignUpFields = () => {
	const [fields, setFields] = useState({
		[SIGN_UP_FIELDS.EMAIL_ADDRESS]: { value: "", error: false },
		[SIGN_UP_FIELDS.PASSWORD]: { value: "", error: false },
		[SIGN_UP_FIELDS.FIRST_NAME]: { value: "", error: false },
		[SIGN_UP_FIELDS.LAST_NAME]: { value: "", error: false },
		[SIGN_UP_FIELDS.DATE_OF_BIRTH]: { value: "", error: false },
		[SIGN_UP_FIELDS.GENDER]: { value: GENDER.NONE, error: false },
	});

	const setValue = (field: SIGN_UP_FIELDS, value: string | GENDER) => {
		setFields((prevFields: any) => ({
			...prevFields,
			[field]: { error: prevFields[field].error, value },
		}));
	};

	const setError = (field: SIGN_UP_FIELDS, error: boolean) => {
		setFields((prevFields: any) => {
			return {
				...prevFields,
				[field]: { error: error, value: prevFields[field].value },
			};
		});
	};
	return {
		emailAddress: fields[SIGN_UP_FIELDS.EMAIL_ADDRESS],
		password: fields[SIGN_UP_FIELDS.PASSWORD],
		firstName: fields[SIGN_UP_FIELDS.FIRST_NAME],
		lastName: fields[SIGN_UP_FIELDS.LAST_NAME],
		dateOfBirth: fields[SIGN_UP_FIELDS.DATE_OF_BIRTH],
		gender: fields[SIGN_UP_FIELDS.GENDER],
		setValue,
		setError,
	};
};
