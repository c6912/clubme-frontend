import { TextInput, HelperText } from "react-native-paper";

type TextFieldType = {
	value: string;
	label: string;
	style: any;
	onChangeText: any;
	error: boolean;
	errorStyle: any;
	errorMsg: string;
};

export const TextField: TextFieldType = ({
	value = "",
	label = "",
	style = {},
	disabled = false,
	onChangeText,
	error = false,
	errorStyle = {},
	errorMsg = "",
}) => {
	return (
		<>
			<TextInput
				disabled={disabled}
				value={value}
				style={style}
				onChangeText={(value) => {
					onChangeText(value);
				}}
				label={label}
				autoComplete={false}
			/>
			{error && (
				<HelperText style={errorStyle} type="error" visible={error}>
					{errorMsg}
				</HelperText>
			)}
		</>
	);
};
