import React from "react";
import { Dimensions, StyleSheet, Text, View } from "react-native";

export const RecommendationCard = ({
	cardName,
	cardPrice,
}: {
	cardName: string;
	cardPrice: string;
}) => {
	return (
		<View style={[styles.item]}>
			<Text style={styles.title}>{cardName}</Text>
			<Text style={styles.price}>Card price: {cardPrice}</Text>
			<View
				style={{
					backgroundColor: "#63A26F",
					borderRadius: 5,
					marginBottom: 5,
				}}
			>
				<Text style={{ color: "white", textAlign: "center", fontSize: 13 }}>
					Membership Card
				</Text>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	item: {
		display: "flex",
		justifyContent: "space-between",
		backgroundColor: "white",
		padding: 5,
		width: (Dimensions.get("window").width - 50) / 2,
		height: 130,
		marginVertical: 3,
		marginHorizontal: 3,
		borderWidth: 1,
		borderRadius: 10,
	},
	title: {
		fontSize: 20,
		color: "black",
		fontWeight: "900",
		fontFamily: "nunito",
		alignSelf: "center",
	},
	price: {
		fontSize: 15,
		color: "grey",
		fontWeight: "700",
		fontFamily: "nunito",
		alignSelf: "center",
	},
});
