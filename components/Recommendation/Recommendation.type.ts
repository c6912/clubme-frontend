export type Recommendation = {
    cardName: string,
    cardPrice: string,
    cardStores: string,
    cardBenefits: string,
    link: string
}

export const adaptRecommendationToCard = (recommendation: Recommendation) => {
    return {
        isRecommendation: true,
        name: recommendation.cardName,
        price: recommendation.cardPrice,
        store: recommendation.cardStores,
        benefits: recommendation.cardBenefits,
        link: recommendation.link
    }
}