import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    notification: {
        height: 65,
        backgroundColor: "rgb(234,239,243)",
        borderRadius: 10,
        marginTop: 5,
        display: "flex",
        flexDirection: "row",
        padding: 8,
        writingDirection: "rtl",
        alignItems: "center"
    },
    notificationText: {
        fontSize: 15,
        marginLeft: 10
    }
});