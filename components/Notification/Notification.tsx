import { FontAwesome } from "@expo/vector-icons";
import React, { FC } from "react";
import { Text, View } from "react-native";
import { styles } from "./Notification.style";

// change this to db type
export type NotificationProps = {text: string };

export const Notification: FC<NotificationProps> = ({
	text
}) => {
    return (
        <View style={styles.notification}>
            <Text style={styles.notificationText}>
                {text}
            </Text>
            <FontAwesome name="bullhorn" size={27}></FontAwesome>
        </View>
    );
}