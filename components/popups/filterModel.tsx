import { FontAwesome, Ionicons } from "@expo/vector-icons";
import Checkbox from "expo-checkbox";
import React, { FC, useState } from "react";
import {
	Dimensions,
	Modal,
	Pressable,
	SafeAreaView,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from "react-native";
import { CARD_BELONGING_TYPES, CATEGORIES } from "../../constants/Categories";

const FilterModal: FC<{
	handleFilterCards: (
		checkedState: boolean[],
		checkedCardType: boolean[]
	) => void;
}> = ({ handleFilterCards }) => {
	const [modalVisible, setModalVisible] = useState(false);
	const [checkedTags, setCheckedTags] = useState<string[]>([]);
	const [checkedCardType, setCheckedCardType] = useState<boolean[]>([
		true,
		false,
		false,
	]);

	const [checkedState, setCheckedState] = useState(
		new Array(CATEGORIES.length).fill(true)
	);

	const handleOnChange = (position: number) => {
		const updatedCheckedState = checkedState.map((item, index) =>
			index === position ? !item : item
		);
		setCheckedState(updatedCheckedState);
		const tempCheckedTags: string[] = [];
		updatedCheckedState.forEach((element, index) => {
			if (element) {
				tempCheckedTags.push(CATEGORIES[index]);
			}
		});
		setCheckedTags(tempCheckedTags);
	};

	const filterModals = (checkedState, checkedCardType) => {
		handleFilterCards(checkedState, checkedCardType);
		setModalVisible(!modalVisible);
	};

	const clearAll = () => {
		setCheckedState(new Array(CATEGORIES.length).fill(false));
		setCheckedTags([]);
	};

	return (
		<View style={styles.modalContainer}>
			<Modal
				animationType="slide"
				visible={modalVisible}
				onRequestClose={() => {
					setModalVisible(!modalVisible);
				}}
			>
				<SafeAreaView>
					<View style={styles.modalContainer}>
						<View
							style={{
								height: Dimensions.get("window").height - 20,
								display: "flex",
								flexDirection: "column",
								justifyContent: "space-between",
							}}
						>
							<SafeAreaView>
								<View style={styles.modalTitleContainer}>
									<Pressable onPress={() => setModalVisible(!modalVisible)}>
										<Ionicons name="close" size={30} color="black"></Ionicons>
									</Pressable>
									<Text style={styles.modalTitle}>Filter</Text>
								</View>
								<View
									style={{
										display: "flex",
										flexDirection: "row",
										justifyContent: "space-around",
									}}
								>
									<View style={{ display: "flex", flexDirection: "column" }}>
										{CATEGORIES.map((type, index) => {
											return (
												<View key={index}>
													<View style={styles.section}>
														<Checkbox
															color="#55aaff"
															style={styles.checkbox}
															value={checkedState[index]}
															onValueChange={() => handleOnChange(index)}
														/>
														<Text style={styles.defaultText}>{type}</Text>
													</View>
												</View>
											);
										})}
									</View>
									<View style={{ display: "flex", flexDirection: "column" }}>
										<View style={styles.section}>
											<Checkbox
												color="#55aaff"
												style={styles.checkbox}
												value={checkedCardType[CARD_BELONGING_TYPES.MINE.id]}
												onValueChange={() => {
													setCheckedCardType((prev) => [
														!prev[0],
														prev[1],
														prev[2],
													]);
												}}
											/>
											<Text style={styles.defaultText}>Mine</Text>
										</View>
										<View style={styles.section}>
											<Checkbox
												color="#55aaff"
												style={styles.checkbox}
												value={checkedCardType[CARD_BELONGING_TYPES.SHARED.id]}
												onValueChange={() => {
													setCheckedCardType((prev) => [
														prev[0],
														!prev[1],
														prev[2],
													]);
												}}
											/>
											<Text style={styles.defaultText}>Shared</Text>
										</View>
										<View style={styles.section}>
											<Checkbox
												color="#55aaff"
												style={styles.checkbox}
												value={
													checkedCardType[CARD_BELONGING_TYPES.EXTERNAL.id]
												}
												onValueChange={() => {
													setCheckedCardType((prev) => [
														prev[0],
														prev[1],
														!prev[2],
													]);
												}}
											/>
											<Text style={styles.defaultText}>External</Text>
										</View>
									</View>
								</View>
								<View style={styles.line} />
							</SafeAreaView>
							<View style={styles.bottomButtons}>
								<Pressable
									style={[styles.clearButton, styles.button]}
									onPress={() => clearAll()}
								>
									<Text style={styles.defaultText}>Clear</Text>
								</Pressable>
								<Pressable
									style={[styles.applyButton, styles.button]}
									onPress={() => filterModals(checkedState, checkedCardType)}
								>
									<Text style={[styles.defaultText, { color: "white" }]}>
										Apply
									</Text>
								</Pressable>
							</View>
						</View>
					</View>
				</SafeAreaView>
			</Modal>
			<TouchableOpacity
				style={styles.openModalButton}
				onPress={() => setModalVisible(true)}
			>
				<Text style={styles.openModalbuttonText}>Filter</Text>
				<FontAwesome name="sliders" size={20} />
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	modalContainer: {
		justifyContent: "center",
		// margin: 15,
		// padding: 8,
	},
	section: {
		flexDirection: "row",
		alignItems: "center",
	},
	checkbox: {
		margin: 8,
	},
	modalTitle: {
		marginBottom: 20,
		marginTop: 5,
		marginLeft: "32%",
		textAlign: "center",
		fontSize: 24,
	},
	modalTitleContainer: {
		marginLeft: 15,
		marginTop: 8,
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-start",
	},
	openModalButton: {
		borderWidth: 1,
		borderColor: "rgba(0,0,0,0.2)",
		alignItems: "center",
		width: 100,
		// position: "absolute",
		height: 38,
		borderRadius: 100,
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
	},
	openModalbuttonText: {
		fontSize: 17,
		paddingRight: 7,
	},
	line: {
		borderBottomColor: "rgb(229, 229, 229)",
		borderBottomWidth: 1,
		marginTop: 15,
	},
	bottomButtons: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-evenly",
	},
	button: {
		color: "white",
		borderRadius: 56,
		textAlign: "center",
		marginTop: 20,
		display: "flex",
		justifyContent: "center",
		flexDirection: "column",
		width: "40%",
		height: 50,
		alignItems: "center",
	},
	applyButton: {
		backgroundColor: "rgb(0, 0, 0)",
	},
	clearButton: {
		borderColor: "rgba(0,0,0,0.2)",
		borderWidth: 1,
	},
	defaultText: {
		fontSize: 17,
	},
});

export default FilterModal;
