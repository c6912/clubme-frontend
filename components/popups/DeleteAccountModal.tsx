import { FontAwesome } from "@expo/vector-icons";
import React, { FC, useContext, useState } from "react";
import { Modal, StyleSheet, Text, Pressable, View } from "react-native";
import { UserContext } from "../../App";
import userService from "../../services/common-beckend/userService";
import { getUser } from "../../utils/token";

const DeleteAccountModal = ({ open, setModalVisible }: any) => {
	const { logout } = useContext(UserContext);

	return (
		<View style={styles.centeredView}>
			<Modal
				animationType="slide"
				transparent={true}
				visible={open}
				onRequestClose={() => {
					setModalVisible(false);
				}}
			>
				<View style={styles.centeredView}>
					<View style={styles.modalView}>
						<Text style={styles.modalText}>Are you sure?</Text>
						<View style={{ display: "flex", flexDirection: "row" }}>
							<Pressable
								style={[styles.button]}
								onPress={() => setModalVisible(false)}
							>
								<Text style={[styles.textCancel, styles.textStyle]}>
									Cancel
								</Text>
							</Pressable>
							<Pressable
								style={[styles.button]}
								onPress={() => {
									getUser().then(({ username }) => {
										userService.delete(username).then(() => {
											logout();
										});
										// setModalVisible(false);
									});
								}}
							>
								<Text style={[styles.textDelete, styles.textStyle]}>
									Delete
								</Text>
							</Pressable>
						</View>
					</View>
				</View>
			</Modal>
		</View>
	);
};

const styles = StyleSheet.create({
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		marginTop: 22,
	},
	modalView: {
		margin: 20,
		backgroundColor: "white",
		borderRadius: 20,
		padding: 35,
		alignItems: "center",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		elevation: 5,
	},
	button: {
		borderRadius: 20,
		padding: 10,
		elevation: 2,
	},
	textStyle: { textAlign: "center", fontSize: 18 },
	textDelete: {
		color: "red",
	},
	textTitle: { fontSize: 23 },
	textCancel: {
		color: "black",
	},
	modalText: {
		marginBottom: 15,
		textAlign: "center",
		fontSize: 23,
	},
});

export default DeleteAccountModal;
