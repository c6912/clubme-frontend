import { FontAwesome, Ionicons } from "@expo/vector-icons";
import { Notification, NotificationProps } from "../Notification/Notification";
import React, { FC, useEffect, useState } from "react";
import {
  FlatList,
  Modal,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { BoldNunText, NunText } from "../StyledText";
import cardService from "../../services/cardService";
import { getUser } from "../../utils/token";
import { SafeAreaView } from "react-native-safe-area-context";

const NotificationsModal: FC<{}> = () => {
  const [modalVisible, setModalVisible] = useState(false);

  const [notifs, setNotifs] = useState<any>([
    { text: "Your Gift card `Yanga` is about to expire in 30 days" },
    {
      text: "Your Membership card `Dream card - Fox home` is about to expire in 11 days",
    },
  ]);

  const [expiredIn] = useState<any>(
    new Date(new Date().setDate(new Date().getDate() - 30))
  );

  const renderItem = ({ item }: { item: NotificationProps }) => (
    <Notification {...item}></Notification>
  );

  const days = (date_1, date_2) => {
    let difference = date_1.getTime() - date_2.getTime();
    let TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
    return TotalDays;
  };

  const isGoingToExpire = (currDate: any) => {
    console.log("here");
    return (
      days(new Date(currDate.expirationDate), new Date()) <= 30 &&
      days(new Date(currDate.expirationDate), new Date()) > 0
    );
  };

  useEffect(() => {
    getUser().then(({ username }) => {
      if (username) {
        cardService.getCards(username).then((user: any) => {
          let tempNotifs = [...notifs];
          user.giftcards.forEach((element) => {
            if (isGoingToExpire(element)) {
              tempNotifs.push({
                text: `The Gift card ${
                  element.name
                } is about to expire in ${days(
                  new Date(element.expirationDate),
                  new Date()
                )} days`,
              });
            }
          });
          user.MembershipCards.forEach((element) => {
            if (isGoingToExpire(element)) {
              tempNotifs.push({
                text: `The Gift card ${
                  element.name
                } is about to expire in ${days(
                  new Date(element.expirationDate),
                  new Date()
                )} days`,
              });
            }
          });
          user.StoreCredits.forEach((element) => {
            if (isGoingToExpire(element)) {
              tempNotifs.push({
                text: `The Gift card ${
                  element.name
                } is about to expire in ${days(
                  new Date(element.expirationDate),
                  new Date()
                )} days`,
              });
            }
          });

          setNotifs(tempNotifs);
        });
      }
    });
  }, []);

  return (
    <View style={styles.modalContainer}>
      <Modal
        animationType={"slide"}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.modalView}>
          <SafeAreaView>
            <View style={styles.header}>
              <BoldNunText style={{ fontSize: 25 }}>Notifications</BoldNunText>
              <Pressable onPress={() => setModalVisible(!modalVisible)}>
                <Ionicons name="close-outline" size={32}></Ionicons>
              </Pressable>
            </View>
            <ScrollView>
              <View style={styles.notificationsContainer}>
                {notifs.length ? (
                  <FlatList
                    data={notifs}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => index.toString()}
                  />
                ) : (
                  <NunText style={styles.noNotificationsText}>
                    There are no notifications
                  </NunText>
                )}
              </View>
            </ScrollView>
          </SafeAreaView>
        </View>
      </Modal>

      <TouchableOpacity
        style={{ width: "fit-content" }}
        onPress={() => setModalVisible(!modalVisible)}
      >
        <FontAwesome size={26} name="bell-o" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    textAlign: "right",
    writingDirection: "rtl",
  },
  modalView: {
    margin: 22,
    flex: 1,
  },
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  notificationsContainer: {
    marginTop: 20,
  },
  noNotificationsText: {
    alignSelf: "center",
    fontSize: 18,
    marginTop: 42,
  },
});

export default NotificationsModal;
