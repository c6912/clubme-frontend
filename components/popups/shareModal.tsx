import { Entypo, FontAwesome, Ionicons } from "@expo/vector-icons";
import type { CardType } from "components/PopupCard/PopupCard.types";
import React, { FC, useState } from "react";
import {
	Modal,
	Pressable,
	StyleSheet,
	TextInput,
	TouchableOpacity,
	View,
} from "react-native";
import { Button } from "react-native-elements";
import { Snackbar } from "react-native-paper";
import shareService from "../../services/common-beckend/shareService";
import { BoldNunText, NunText } from "../StyledText";

const ShareModal: FC<{
	cardName: string;
	cardId: string;
	cardType: CardType;
}> = ({ cardName, cardId, cardType }) => {
	const [modalVisible, setModalVisible] = useState(false);
	const [userEmailToShareWith, onChangeEmail] = useState("");

	const [alertText, setAlertText] = useState("");
	const [visible, setVisible] = useState(false);
	const onDismissSnackBar = () => setVisible(false);

	const shareCard = () => {
		if (userEmailToShareWith && userEmailToShareWith != "") {
			shareService
				.shareCard(userEmailToShareWith, cardId, cardType)
				.then(() => {
					setAlertText(`Card was shared`);
					setVisible(true);
					setTimeout(() => {
						setModalVisible(!modalVisible);
					}, 1500);
				})
				.catch((error) => {
					if (
						error.response?.status === 400 ||
						error.response?.status === 409
					) {
						setAlertText(`Share Failed - ${error.response?.data}`);
					} else {
						setAlertText(`Share Failed`);
					}
					setVisible(true);
				});
		}
	};

	return (
		<View style={styles.modalContainer}>
			<Modal
				animationType={"slide"}
				visible={modalVisible}
				onRequestClose={() => {
					setModalVisible(!modalVisible);
				}}
			>
				<View style={styles.modalView}>
					<View style={styles.header}>
						<BoldNunText style={{ fontSize: 25 }}>Share card</BoldNunText>
						<Pressable onPress={() => setModalVisible(!modalVisible)}>
							<Ionicons name="close-outline" size={32}></Ionicons>
						</Pressable>
					</View>

					<View style={styles.modalData}>
						<BoldNunText style={{ fontSize: 27 }}>{cardName}</BoldNunText>
						<NunText
							style={{ margin: 20, fontSize: 19, marginTop: 82, width: 230 }}
						>
							Enter a user's email to share with
						</NunText>
						<TextInput
							style={styles.emailInput}
							onChangeText={onChangeEmail}
							value={userEmailToShareWith}
							placeholder="Email"
							placeholderTextColor="grey"
						/>
						<Button
							onPress={() => shareCard()}
							icon={<FontAwesome name="share" size={21} style={{}} />}
							buttonStyle={styles.shareButton}
							style={{
								width: "fit-content",
								alignSelf: "center",
							}}
						/>
						<Snackbar
							duration={3000}
							visible={visible}
							onDismiss={onDismissSnackBar}
						>
							{alertText}
						</Snackbar>
					</View>
				</View>
			</Modal>

			<TouchableOpacity
				style={{ width: "100%", alignSelf: "center" }}
				onPress={() => setModalVisible(!modalVisible)}
			>
				<Entypo name="share" size={23} style={styles.openModalButton} />
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	modalContainer: {
		textAlign: "right",
		writingDirection: "rtl",
	},
	modalView: {
		margin: 22,
		flex: 1,
	},
	modalData: {
		textAlign: "center",
		display: "flex",
		flexDirection: "column",
		height: "85%",
		margin: "auto",
	},
	header: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		borderBottomColor: "#bcbaba",
		borderBottomWidth: 1,
	},
	openModalButton: {},
	emailInput: {
		height: 40,
		width: 238,
		margin: 12,
		borderWidth: 1,
		padding: 10,
		alignSelf: "center",
		borderRadius: 6,
		fontSize: 17,
		fontFamily: "nunito",
	},
	shareButton: {
		backgroundColor: "rgb(182, 186, 183)",
		borderRadius: 20,
		marginTop: 10,
	},
});

export default ShareModal;
