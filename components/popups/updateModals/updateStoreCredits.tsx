import React, { FC, useState } from "react";
import {
	Modal,
	Pressable,
	SafeAreaView,
	StyleSheet,
	Text,
	TextInput,
	View,
} from "react-native";

// hooks
import { useStoreCredits } from "../../../context/StoreCredits/storeCredits-context";
import { useNavigation } from "@react-navigation/native";

// interfaces
import { IStoreCredits } from "../../StoreCredit/IStoreCredits";
import storeCreditService from "../../../services/common-beckend/storeCreditService";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const UpdateStoreCredits: FC<Partial<IStoreCredits>> = ({
	id,
	amount,
	expirationDate,
	image,
	onClose,
	purchaseDate,
	store,
	name,
}) => {
	const { storeCredits, setStoreCredits } = useStoreCredits();

	const [modalVisible, setModalVisible] = useState(false);

	const [storeCreditExpDate, setStoreCreditExpDate] = useState(
		new Date(expirationDate).toLocaleDateString()
	);
	const [storeCreditPurchaseDate, setStoreCreditPurchaseDate] = useState(
		new Date(purchaseDate).toLocaleDateString()
	);
	const [storeCreditAmount, setStoreCreditAmount] = useState(amount.toString());
	const [storeCreditStore, setStoreCreditStore] = useState(store);
	const [storeCreditName, setStoreCreditName] = useState(name);

	const navigation = useNavigation();

	const handleUpdate = () => {
		let tempStoreCredits = [...storeCredits];
		tempStoreCredits.forEach((element) => {
			if (element.id === id) {
				element.amount = +storeCreditAmount;
				element.expirationDate = new Date(
					storeCreditExpDate
				).toLocaleDateString();
				element.purchaseDate = new Date(
					storeCreditPurchaseDate
				).toLocaleDateString();
				element.store = storeCreditStore;
				element.name = storeCreditName;

				storeCreditService.update(
					{
						expirationDate: new Date(element.expirationDate),
						name: element.name,
						store: element.store,
						amount: element.amount,
						purchaseDate: new Date(element.purchaseDate),
					},
					id
				);
				return;
			}
		});

		setStoreCredits(tempStoreCredits);

		onClose();

		setModalVisible(!modalVisible);
	};

	return (
		<View style={styles.centeredView}>
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					alert("Modal has been closed.");
					setModalVisible(!modalVisible);
				}}
			>
				<View style={styles.centeredView}>
					<View style={styles.modalView}>
						<Text style={styles.modalText}>Update Your store credit</Text>
						<SafeAreaView>
							<Text>Store:</Text>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditStore}
								value={storeCreditStore}
								placeholder={"store"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditName}
								value={storeCreditName}
								placeholder={"name"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditAmount}
								value={storeCreditAmount}
								placeholder={"amount"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditExpDate}
								value={storeCreditExpDate}
								placeholder={"Expiration Date"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditPurchaseDate}
								value={storeCreditPurchaseDate}
								placeholder={"Purchase Date"}
								placeholderTextColor="black"
							/>
						</SafeAreaView>
						<View style={{ flexDirection: "row", flexWrap: "wrap" }}>
							<Pressable
								style={[styles.button, styles.buttonAdd]}
								onPress={() => handleUpdate()}
							>
								<Text style={styles.add}>add</Text>
							</Pressable>
							<Pressable
								style={[styles.button, styles.buttonCancel]}
								onPress={() => setModalVisible(!modalVisible)}
							>
								<Text style={styles.textStyle}>cancel</Text>
							</Pressable>
						</View>
					</View>
				</View>
			</Modal>
			<Pressable style={[]} onPress={() => setModalVisible(true)}>
				<MaterialCommunityIcons
					name="pencil"
					size={27}
				></MaterialCommunityIcons>
			</Pressable>
		</View>
	);
};

const styles = StyleSheet.create({
	input: {
		height: 40,
		width: 200,
		margin: 12,
		borderWidth: 1,
		padding: 10,
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "flex-start",
	},
	modalView: {
		margin: 20,
		backgroundColor: "white",
		borderRadius: 20,
		padding: 35,
		alignItems: "center",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		elevation: 5,
	},
	button: {
		borderRadius: 20,
		padding: 10,
		marginLeft: 20,
		marginRight: 20,
		elevation: 2,
		marginBottom: 20,
	},
	buttonUpdate: {
		backgroundColor: "black",
		width: 230,
	},
	buttonOpen: {
		backgroundColor: "#F194FF",
	},
	buttonAdd: {
		backgroundColor: "white",
		width: 60,
		// border: '0.5px solid lightgrey'
		borderWidth: 0.5,
		borderColor: "lightgrey",
	},
	buttonCancel: {
		backgroundColor: "black",
		width: 60,
	},
	textStyle: {
		color: "white",
		fontWeight: "bold",
		textAlign: "center",
	},
	add: {
		fontWeight: "bold",
		textAlign: "center",
		color: "black",
	},
	modalText: {
		marginBottom: 15,
		textAlign: "center",
		fontSize: 20,
		fontWeight: "bold",
	},
});

export default UpdateStoreCredits;
