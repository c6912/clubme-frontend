import React, { FC, useState } from "react";
import {
	Modal,
	Pressable,
	SafeAreaView,
	StyleSheet,
	Text,
	TextInput,
	View,
} from "react-native";

// hooks
import { useGiftCards } from "../../../context/giftCards/giftcards-context";
import { useNavigation } from "@react-navigation/native";

// interfaces
import { GiftCardProps } from "../../GiftCard/GiftCard.types";
import giftcardService from "../../../services/common-beckend/giftcardService";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const UpdateGiftcards: FC<Partial<GiftCardProps>> = ({
	id,
	amount,
	expirationDate,
	purchaseDate,
	store,
	onClose,
	name,
}) => {
	const { giftCards, setGiftCards } = useGiftCards();

	const [modalVisible, setModalVisible] = useState(false);

	const [giftcardExpDate, setgiftcardExpDate] = useState(
		new Date(expirationDate).toLocaleDateString()
	);
	const [giftcardPurchaseDate, setgiftcardPurchaseDate] = useState(
		new Date(purchaseDate).toLocaleDateString()
	);
	const [giftcardAmount, setgiftcardAmount] = useState(amount.toString());
	const [giftcardStore, setgiftcardStore] = useState(store);
	const [giftcardName, setgiftcardName] = useState(name);

	const navigation = useNavigation();

	const handleUpdate = () => {
		let tempgiftcards = [...giftCards];
		tempgiftcards.forEach((element) => {
			if (element.id === id) {
				element.amount = +giftcardAmount;
				element.expirationDate = new Date(giftcardExpDate).toLocaleDateString();
				element.purchaseDate = new Date(
					giftcardPurchaseDate
				).toLocaleDateString();
				element.store = giftcardStore;
				element.name = giftcardName;

				giftcardService.update(
					{
						expirationDate: new Date(element.expirationDate),
						name: element.name,
						store: element.store,
						points: element.amount,
						purchaseDate: new Date(element.purchaseDate),
					},
					id
				);
				return;
			}
		});

		setGiftCards(tempgiftcards);

		onClose();

		setModalVisible(!modalVisible);
	};

	return (
		<View style={styles.centeredView}>
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					alert("Modal has been closed.");
					setModalVisible(!modalVisible);
				}}
			>
				<View style={styles.centeredView}>
					<View style={styles.modalView}>
						<Text style={styles.modalText}>Update Your store credit</Text>
						<SafeAreaView>
							<Text>Store:</Text>
							<TextInput
								style={styles.input}
								onChangeText={setgiftcardStore}
								value={giftcardStore}
								placeholder={"store"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setgiftcardName}
								value={giftcardName}
								placeholder={"name"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setgiftcardAmount}
								value={giftcardAmount}
								placeholder={"amount"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setgiftcardExpDate}
								value={giftcardExpDate}
								placeholder={"Expiration Date"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setgiftcardPurchaseDate}
								value={giftcardPurchaseDate}
								placeholder={"Purchase Date"}
								placeholderTextColor="black"
							/>
						</SafeAreaView>
						<View style={{ flexDirection: "row", flexWrap: "wrap" }}>
							<Pressable
								style={[styles.button, styles.buttonAdd]}
								onPress={() => handleUpdate()}
							>
								<Text style={styles.add}>add</Text>
							</Pressable>
							<Pressable
								style={[styles.button, styles.buttonCancel]}
								onPress={() => setModalVisible(!modalVisible)}
							>
								<Text style={styles.textStyle}>cancel</Text>
							</Pressable>
						</View>
					</View>
				</View>
			</Modal>
			<Pressable style={[]} onPress={() => setModalVisible(true)}>
				<MaterialCommunityIcons
					name="pencil"
					size={27}
				></MaterialCommunityIcons>
			</Pressable>
		</View>
	);
};

const styles = StyleSheet.create({
	input: {
		height: 40,
		width: 200,
		margin: 12,
		borderWidth: 1,
		padding: 10,
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",

		alignSelf: "flex-start",
	},
	modalView: {
		margin: 20,
		backgroundColor: "white",
		borderRadius: 20,
		padding: 35,
		alignItems: "center",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		elevation: 5,
	},
	button: {
		borderRadius: 20,
		padding: 10,
		marginLeft: 20,
		marginRight: 20,
		elevation: 2,
		marginBottom: 20,
	},
	add: {
		fontWeight: "bold",
		textAlign: "center",
		color: "black",
	},
	buttonUpdate: {
		backgroundColor: "black",
		width: 230,
	},
	buttonOpen: {
		backgroundColor: "#F194FF",
	},
	buttonAdd: {
		backgroundColor: "white",
		width: 60,
		borderWidth: 0.5,
		borderColor: "lightgrey",
	},
	buttonCancel: {
		backgroundColor: "black",
		width: 60,
	},
	textStyle: {
		color: "white",
		fontWeight: "bold",
		textAlign: "center",
	},
	modalText: {
		marginBottom: 15,
		textAlign: "center",
		fontSize: 20,
		fontWeight: "bold",
	},
});

export default UpdateGiftcards;
