import { useNavigation } from "@react-navigation/native";
import {
	Entypo,
	Ionicons,
	MaterialCommunityIcons,
	MaterialIcons,
} from "@expo/vector-icons";
import React, { FC, useState } from "react";
import {
	Modal,
	Pressable,
	SafeAreaView,
	StyleSheet,
	Text,
	TextInput,
	View,
} from "react-native";
import { useMembershipCards } from "../../../context/MembershipCards/membershipcards-context";
import { IMembership } from "../../MembershipCard/Membership.types";

import membershipService from "../../../services/common-beckend/membershipService";

const UpdateMembershipCardModal: FC<Partial<IMembership>> = ({
	id,
	store,
	expirationDate,
	purchaseDate,
	points,
	onClose,
	name,
}) => {
	const { membershipCards, setMembershipCards } = useMembershipCards();

	const [modalVisible, setModalVisible] = useState(false);

	const [storeCreditExpDate, setStoreCreditExpDate] = useState(
		new Date(expirationDate).toLocaleDateString()
	);
	const [storeCreditPurchaseDate, setStoreCreditPurchaseDate] = useState(
		new Date(purchaseDate).toLocaleDateString()
	);
	const [storeCreditAmount, setStoreCreditAmount] = useState(points.toString());
	const [storeCreditStore, setStoreCreditStore] = useState(store);
	const [storeCreditName, setStoreCreditName] = useState(name);

	const navigation = useNavigation();

	const handleUpdate = () => {
		let tempStoreCredits = [...membershipCards];
		tempStoreCredits.forEach((element) => {
			if (element.id === id) {
				element.points = +storeCreditAmount;
				element.expirationDate = new Date(
					storeCreditExpDate
				).toLocaleDateString();
				element.purchaseDate = new Date(
					storeCreditPurchaseDate
				).toLocaleDateString();
				element.store = storeCreditStore;
				element.name = storeCreditName;

				membershipService.update(
					{
						expirationDate: new Date(element.expirationDate),
						name: element.name,
						store: element.store,
						points: element.points,
						purchaseDate: new Date(element.purchaseDate),
					},
					id
				);
				return;
			}
		});

		setMembershipCards(tempStoreCredits);

		onClose();

		setModalVisible(!modalVisible);
	};

	return (
		<View style={styles.centeredView}>
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					alert("Modal has been closed.");
					setModalVisible(!modalVisible);
				}}
			>
				<View style={styles.centeredView}>
					<View style={styles.modalView}>
						<Text style={styles.modalText}>Update Your store credit</Text>
						<SafeAreaView>
							<Text>Store:</Text>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditStore}
								value={storeCreditStore}
								placeholder={"store"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditName}
								value={storeCreditName}
								placeholder={"name"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditAmount}
								value={storeCreditAmount}
								placeholder={"amount"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditExpDate}
								value={storeCreditExpDate}
								placeholder={"Expiration Date"}
								placeholderTextColor="black"
							/>
							<TextInput
								style={styles.input}
								onChangeText={setStoreCreditPurchaseDate}
								value={storeCreditPurchaseDate}
								placeholder={"Purchase Date"}
								placeholderTextColor="black"
							/>
						</SafeAreaView>
						<View style={{ flexDirection: "row", flexWrap: "wrap" }}>
							<Pressable
								style={[styles.button, styles.buttonAdd]}
								onPress={() => handleUpdate()}
							>
								<Text style={styles.add}>add</Text>
							</Pressable>
							<Pressable
								style={[styles.button, styles.buttonCancel]}
								onPress={() => setModalVisible(!modalVisible)}
							>
								<Text style={styles.textStyle}>cancel</Text>
							</Pressable>
						</View>
					</View>
				</View>
			</Modal>
			<Pressable style={[]} onPress={() => setModalVisible(true)}>
				<MaterialCommunityIcons
					name="pencil"
					size={27}
				></MaterialCommunityIcons>
			</Pressable>
		</View>
	);
};

const styles = StyleSheet.create({
	input: {
		height: 40,
		width: 200,
		margin: 12,
		borderWidth: 1,
		padding: 10,
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "flex-start",
	},
	modalView: {
		margin: 20,
		backgroundColor: "white",
		borderRadius: 20,
		padding: 35,
		alignItems: "center",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		elevation: 5,
	},
	button: {
		borderRadius: 20,
		padding: 10,
		marginLeft: 20,
		marginRight: 20,
		elevation: 2,
		marginBottom: 20,
	},
	buttonUpdate: {
		backgroundColor: "black",
		width: 230,
	},
	buttonOpen: {
		backgroundColor: "#F194FF",
	},
	buttonAdd: {
		backgroundColor: "white",
		width: 60,
		// border: '0.5px solid lightgrey'
		borderWidth: 0.5,
		borderColor: "lightgrey",
	},
	buttonCancel: {
		backgroundColor: "black",
		width: 60,
	},
	textStyle: {
		color: "white",
		fontWeight: "bold",
		textAlign: "center",
	},
	add: {
		fontWeight: "bold",
		textAlign: "center",
		color: "black",
	},
	modalText: {
		marginBottom: 15,
		textAlign: "center",
		fontSize: 20,
		fontWeight: "bold",
	},
});

export default UpdateMembershipCardModal;
