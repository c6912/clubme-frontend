import { FontAwesome } from "@expo/vector-icons";
import Checkbox from "expo-checkbox";
import React, { FC, useState } from "react";
import {
	Modal,
	StyleSheet,
	Text,
	Pressable,
	View,
	TouchableOpacity,
	TextInput,
	SafeAreaView,
	Dimensions,
} from "react-native";
import { CATEGORIES } from "../../../constants/Categories";
import giftcardService from "../../../services/common-beckend/giftcardService";
import membershipService from "../../../services/common-beckend/membershipService";
import { getUser } from "../../../utils/token";
import { IMembership } from "../../MembershipCard/Membership.types";
import { TextField } from "../../TextField/TextField";

import { useMembershipCards } from "../../../context/MembershipCards/membershipcards-context";

const AddMembershipModal: FC = () => {
	const [modalVisible, setModalVisible] = useState(false);

	const { membershipCards, setMembershipCards } = useMembershipCards();

	const [name, onChangeName] = React.useState("");

	const [store, onChangeStore] = React.useState("");

	const [points, onChangePoints] = React.useState("");

	const [purchaseDate, onChangePurchaseDate] = React.useState("");

	const [expDate, onChangeExpDate] = React.useState("");

	const [checkedState, setCheckedState] = useState(
		new Array(CATEGORIES.length).fill(false)
	);

	const handleAddNewModel = () => {
		if (store && points && purchaseDate && expDate && name && checkedState) {
			getUser()
				.then(({ username }: { username: string }) => {
					membershipService.create({
						email: username,
						store,
						name,
						purchaseDate: new Date(purchaseDate),
						expirationDate: new Date(expDate),
						points: +points,
						categories: checkedState,
					});

					let tempGiftCardItems = [...membershipCards];
					tempGiftCardItems.push({
						id: username,
						points: +points,
						expirationDate: expDate,
						isFavorite: false,
						name,
						store,
						purchaseDate,
						mainUserId: null,
						categories: checkedState,
					});

					onChangeName("");
					onChangeStore("");
					onChangePoints("");
					onChangePurchaseDate("");
					onChangeExpDate("");

					setMembershipCards(tempGiftCardItems);
				})
				.catch((error) => {
					console.log({ error });
				});
			setModalVisible(!modalVisible);
		} else {
			alert("all fields are required");
		}
	};

	return (
		<View style={styles.centeredView}>
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					setModalVisible(!modalVisible);
				}}
			>
				<View style={styles.centeredView}>
					<View style={styles.modalView}>
						<Text style={styles.modalText}>Add a Membership Card</Text>
						<SafeAreaView>
							<TextField
								value={name}
								style={styles.input}
								label="Name"
								placeholderTextColor="black"
								onChangeText={onChangeName}
							/>
							<TextField
								value={store}
								style={styles.input}
								label="Store"
								placeholderTextColor="black"
								onChangeText={onChangeStore}
							/>
							<TextField
								value={points}
								style={styles.input}
								label="Amount"
								placeholderTextColor="black"
								onChangeText={onChangePoints}
							/>
							<TextField
								value={expDate}
								style={styles.input}
								label="Exp Date (dd/mm/yyyy)"
								placeholderTextColor="black"
								onChangeText={onChangeExpDate}
							/>
							<TextField
								value={purchaseDate}
								style={styles.input}
								label="Purchase Date (dd/mm/yyyy)"
								placeholderTextColor="black"
								onChangeText={onChangePurchaseDate}
							/>
							<View
								style={{
									display: "flex",
									flexDirection: "column",
									justifyContent: "flex-start",
								}}
							>
								<Text
									style={{
										// marginBottom: 5,
										marginTop: 5,
										marginLeft: 5,
										textAlign: "left",
										fontSize: 17,
										fontWeight: "bold",
									}}
								>
									Categories
								</Text>
								<View
									style={{
										display: "grid",
										gridTemplateColumns: "40% 40%",
										marginTop: 15,
									}}
								>
									{CATEGORIES.map((type, index) => {
										return (
											<View key={index}>
												<View style={styles.section}>
													<Checkbox
														color="#55aaff"
														style={styles.checkbox}
														value={checkedState[index]}
														onValueChange={() => {
															setCheckedState((prev) => {
																const newChecked = [...prev];
																newChecked[index] = !newChecked[index];
																return newChecked;
															});
														}}
													/>
													<Text style={styles.defaultText}>{type}</Text>
												</View>
											</View>
										);
									})}
								</View>
							</View>
						</SafeAreaView>
						<View style={styles.bottomButtons}>
							<Pressable
								style={[styles.clearButton, styles.button]}
								onPress={() => setModalVisible(!modalVisible)}
							>
								<Text style={styles.defaultText}>Cancel</Text>
							</Pressable>
							<Pressable
								style={[styles.applyButton, styles.button]}
								onPress={() => handleAddNewModel()}
							>
								<Text style={[styles.defaultText, { color: "white" }]}>
									Add
								</Text>
							</Pressable>
						</View>
					</View>
				</View>
			</Modal>
			<TouchableOpacity
				style={{
					borderWidth: 1,
					borderColor: "rgba(0,0,0,0.2)",
					alignItems: "center",
					justifyContent: "center",
					width: 70,
					position: "absolute",
					bottom: 30,
					left: 10,
					height: 70,
					backgroundColor: "#fff",
					borderRadius: 100,
					alignSelf: "flex-start",
				}}
				onPress={() => setModalVisible(true)}
			>
				<FontAwesome name="plus" size={30} color="#586589" />
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	section: {
		flexDirection: "row",
		alignItems: "center",
	},
	checkbox: {
		margin: 8,
	},
	input: {
		// height: 40,
		// width: Dimensions.get("window").width - 40,
		// margin: 12,
		// borderWidth: 1,
		// padding: 10,
		width: Dimensions.get("window").width - 50,
		margin: 5,
		height: 55,
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		// marginTop: 22,
		alignSelf: "flex-start",
	},
	modalView: {
		margin: 20,
		backgroundColor: "white",
		borderRadius: 20,
		paddingTop: 20,
		alignItems: "center",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		elevation: 5,
	},
	button: {
		color: "white",
		borderRadius: 56,
		textAlign: "center",
		marginTop: 20,
		display: "flex",
		justifyContent: "center",
		flexDirection: "column",
		width: "40%",
		height: 50,
		alignItems: "center",
	},
	buttonOpen: {
		backgroundColor: "#F194FF",
	},
	buttonAdd: {
		backgroundColor: "rgb(0, 0, 0)",
	},
	buttonCancel: {
		borderColor: "rgba(0,0,0,0.2)",
		borderWidth: 1,
	},
	textStyle: {
		color: "white",
		fontWeight: "bold",
		textAlign: "center",
	},
	modalText: {
		marginBottom: 15,
		textAlign: "center",
		fontSize: 20,
		fontWeight: "bold",
	},
	bottomButtons: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-evenly",
		width: Dimensions.get("window").width - 40,
		marginBottom: 10,
	},

	applyButton: {
		backgroundColor: "rgb(0, 0, 0)",
	},
	clearButton: {
		borderColor: "rgba(0,0,0,0.2)",
		borderWidth: 1,
	},
	defaultText: {
		fontSize: 17,
	},
});

export default AddMembershipModal;
