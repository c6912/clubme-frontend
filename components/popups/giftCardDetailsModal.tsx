import {
	Entypo,
	Ionicons,
	MaterialCommunityIcons,
	MaterialIcons,
} from "@expo/vector-icons";
import React, { FC, useEffect, useState } from "react";
import { Modal, Pressable, StyleSheet, Text, View } from "react-native";
import userService from "../../services/common-beckend/userService";
import { cardStyles } from "../PopupCard/PopupCard.style";
import { CardTypes } from "../PopupCard/PopupCard.types";
import { BoldNunText } from "../StyledText";
import ShareModal from "./shareModal";
import { useNavigation } from "@react-navigation/native";

import giftcardService from "../../services/common-beckend/giftcardService";

import { useGiftCards } from "../../context/giftCards/giftcards-context";

import UpdateGiftcards from "./updateModals/updateGiftCardModal";

export const GiftCardDetailsModal: FC<any> = ({
	open,
	onClose = () => {},
	expirationDate,
	id,
	name,
	store,
	amount,
	type,
	external = false,
	isSharedCard = false,
	mainUserId,
}) => {
	const { giftCards, setGiftCards } = useGiftCards();

	const [sharedUserFullName, setSharedUserFullName] = useState<string>("");

	const navigation = useNavigation();

	useEffect(() => {
		if (isSharedCard) {
			userService.findById(mainUserId).then(({ firstName, lastName }: any) => {
				setSharedUserFullName(`${firstName} ${lastName}`);
			});
		}
	}, []);

	const handleDelete = () => {
		let tempGiftCardItems = [...giftCards];
		tempGiftCardItems = tempGiftCardItems.filter(
			(element) => element.id !== id
		);
		setGiftCards(tempGiftCardItems);
		giftcardService.delete(id);
		onClose();
	};

	return (
		<View style={styles.modalContainer}>
			<Modal
				animationType="slide"
				transparent={true}
				visible={open}
				onRequestClose={() => {
					onClose();
				}}
			>
				<View style={styles.modalView}>
					<View style={styles.header}>
						<BoldNunText style={{ fontSize: 25, margin: 5 }}>
							{store}
						</BoldNunText>
						<Pressable onPress={() => onClose()}>
							<Ionicons name="close-outline" size={32}></Ionicons>
						</Pressable>
					</View>
					<View style={{ alignItems: "center", marginTop: 20 }}>
						{type == CardTypes.GiftCard ? (
							<MaterialIcons name="card-giftcard" size={30}></MaterialIcons>
						) : type == CardTypes.MembershipCard ? (
							<MaterialIcons name="card-membership" size={30}></MaterialIcons>
						) : (
							<Entypo name="credit" size={30}></Entypo>
						)}
						<Text
							style={{
								textAlign: "center",
								fontSize: 23,
								fontFamily: "nunito",
							}}
						>
							{type}
						</Text>
					</View>
					<View style={styles.modalData}>
						<View style={{ display: "flex", flexDirection: "row" }}>
							<Text style={cardStyles.property}>Description: </Text>
							<Text style={styles.propValue}> {name}</Text>
						</View>

						{!external && (
							<>
								<View style={{ display: "flex", flexDirection: "row" }}>
									<Text style={cardStyles.property}>Amount: </Text>
									<Text style={styles.propValue}> {amount}</Text>
								</View>
								<View style={{ display: "flex", flexDirection: "row" }}>
									<Text style={cardStyles.property}>Expiration Date: </Text>
									<Text style={styles.propValue}>
										{new Date(expirationDate).toLocaleDateString()}
									</Text>
								</View>

								{!isSharedCard ? (
									<View style={styles.options}>
										<Pressable style={[]} onPress={() => handleDelete()}>
											<MaterialCommunityIcons
												name="delete"
												size={27}
											></MaterialCommunityIcons>
										</Pressable>
										<UpdateGiftcards
											expirationDate={expirationDate}
											id={id}
											name={name}
											store={store}
											amount={amount}
											onClose={onClose}
										/>
										<ShareModal cardName={name} cardId={id} cardType={type} />
									</View>
								) : (
									<View>
										<Text style={styles.sharedByText}>
											shared by - {sharedUserFullName}
										</Text>
									</View>
								)}
							</>
						)}
					</View>
				</View>
			</Modal>
		</View>
	);
};

const styles = StyleSheet.create({
	modalContainer: {
		flex: 1,
		textAlign: "right",
		writingDirection: "rtl",
	},
	input: {
		height: 40,
		width: 200,
		margin: 12,
		borderWidth: 1,
		padding: 10,
	},
	header: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		borderBottomColor: "#bcbaba",
		borderBottomWidth: 1,
		margin: 10,
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		// marginTop: 22,
	},
	modalView: {
		// margin: 22,
		flex: 1,
		backgroundColor: "white",
	},
	modalData: {
		margin: 37,
		borderColor: "#d8d3d3",
		borderWidth: 1,
		borderRadius: 20,
		padding: 14,
	},
	button: {
		borderRadius: 20,
		padding: 10,
		marginLeft: 20,
		marginRight: 20,
		elevation: 2,
	},
	buttonOpen: {
		backgroundColor: "#F194FF",
	},
	buttonAdd: {
		backgroundColor: "red",
	},
	buttonCancel: {
		backgroundColor: "red",
	},
	textStyle: {
		color: "white",
		fontWeight: "bold",
		textAlign: "center",
	},
	modalText: {
		marginBottom: 15,
		textAlign: "center",
		fontSize: 20,
		fontWeight: "bold",
	},
	propValue: {
		fontSize: 20,
		fontFamily: "nunito-bold",
	},
	options: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-evenly",
		marginTop: 20,
	},
	sharedByText: {
		alignSelf: "center",
		fontSize: 16,
		margin: 11,
		color: "#696d6d",
		fontFamily: "nunito",
	},
});
