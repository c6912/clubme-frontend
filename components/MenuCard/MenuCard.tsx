import React from "react";
import { Dimensions, StyleSheet, Text, View } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native";
import { CardType, CardTypes } from "../PopupCard/PopupCard.types";

export const Card = ({
	title,
	isFavorite,
	onFavoriteClick,
	store,
	type,
	shouldShowFavorite = true,
	shared,
}: {
	title: string;
	store: string;
	isFavorite: boolean;
	onFavoriteClick: Function;
	type: CardType;
	shouldShowFavorite: boolean;
	shared: boolean;
}) => {
	const tagColor = {
		backgroundColor:
			type === CardTypes.GiftCard
				? "#F09F20"
				: type === CardTypes.MembershipCard
				? "#63A26F"
				: "#045253",
	};
	const tagText =
		type === CardTypes.GiftCard
			? "Gift Card"
			: type === CardTypes.MembershipCard
			? "Membership Card"
			: "Store Credit";
	const borderColor = "#CBC9CC";

	return (
		<View style={[styles.item, { borderColor }]}>
			<View>
				<Text style={styles.title}>{store}</Text>
				{shared !== undefined && <Text style={styles.store}>{title}</Text>}
			</View>
			<View>
				<View
					style={{
						borderRadius: 5,
						marginBottom: 5,
						...tagColor,
					}}
				>
					<Text style={{ color: "white", textAlign: "center", fontSize: 13 }}>
						{tagText}
					</Text>
				</View>
				{shouldShowFavorite && (
					<View>
						{shared ? (
							<View
								style={{
									backgroundColor: "#2C545A",
									borderRadius: 5,
									marginBottom: 5,
								}}
							>
								<Text
									style={{ color: "white", textAlign: "center", fontSize: 13 }}
								>
									Shared
								</Text>
							</View>
						) : shared !== undefined ? (
							<View
								style={{
									backgroundColor: "#7B75B4",
									borderRadius: 5,
									marginBottom: 5,
								}}
							>
								<Text
									style={{ color: "white", textAlign: "center", fontSize: 13 }}
								>
									Mine
								</Text>
							</View>
						) : (
							<View
								style={{
									backgroundColor: "#9B3E63",
									borderRadius: 5,
									marginBottom: 5,
								}}
							>
								<Text
									style={{ color: "white", textAlign: "center", fontSize: 13 }}
								>
									External
								</Text>
							</View>
						)}
					</View>
				)}
				{shouldShowFavorite ? (
					<View
						style={{
							display: "flex",
							flexDirection: "row",
							justifyContent: "end",
						}}
					>
						<TouchableOpacity
							onPress={() => {
								onFavoriteClick();
							}}
						>
							<FontAwesome
								style={{ alignSelf: "flex-end" }}
								color="#586589"
								size={20}
								name={isFavorite ? "star" : "star-o"}
							/>
						</TouchableOpacity>
					</View>
				) : null}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	item: {
		display: "flex",
		justifyContent: "space-between",
		backgroundColor: "white",
		padding: 5,
		width: (Dimensions.get("window").width - 50) / 2,
		height: 130,
		marginVertical: 3,
		marginHorizontal: 3,
		borderWidth: 1,
		borderRadius: 10,
	},
	title: {
		fontSize: 18,
		color: "black",
		fontWeight: "900",
	},
	store: {
		fontSize: 16,
		color: "grey",
		fontWeight: "700",
	},
});
