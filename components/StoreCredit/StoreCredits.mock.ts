
import { IStoreCredits } from "./IStoreCredits";

export const storeCredit1: IStoreCredits = {
    id: 22,
    name: "Renuar",
    store: "H&M",
    expirationDate: new Date(),
    purchaseDate: new Date(),
    isFavorite: true,
    amount: 310,
    companyId: 24,
    image: '',
    mainUserId: 24
};

export const storeCredit2: IStoreCredits = {
    id: 21,
    name: "H&M",
    store: "H&M",
    expirationDate: new Date(),
    purchaseDate: new Date(),
    isFavorite: true,
    amount: 310,
    companyId: 24,
    image: '',
    mainUserId: 24
};

export const storeCredit3: IStoreCredits = {
    id: 32,
    name: "H&O",
    store: "H&M",
    expirationDate: new Date(),
    purchaseDate: new Date(),
    isFavorite: true,
    amount: 310,
    companyId: 24,
    image: '',
    mainUserId: 24
};

export const storeCredit4: IStoreCredits = {
    id: 25,
    name: "shahar",
    store: "H&M",
    expirationDate: new Date(),
    purchaseDate: new Date(),
    isFavorite: true,
    amount: 310,
    companyId: 24,
    image: '',
    mainUserId: 24
};
export const storeCredit5: IStoreCredits = {
    id: 27,
    name: "Castro",
    store: "H&M",
    expirationDate: new Date(),
    purchaseDate: new Date(),
    isFavorite: true,
    amount: 310,
    companyId: 24,
    image: '',
    mainUserId: 24

};
export const storeCredit6: IStoreCredits = {
    id: 212,
    name: "H&M",
    store: "H&M",
    expirationDate: new Date(),
    purchaseDate: new Date(),
    isFavorite: true,
    amount: 310,
    companyId: 24,
    image: '',
    mainUserId: 24
};


export const StoreCreditMock: IStoreCredits[] = [
    storeCredit1,
    storeCredit2,
    storeCredit3,
    storeCredit4,
    storeCredit5,
    storeCredit6,
];
