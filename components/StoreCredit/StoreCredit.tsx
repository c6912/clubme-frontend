import React, { FC } from "react";

// style
import { Text, Pressable, View } from "react-native";
import { cardStyles } from "../PopupCard/PopupCard.style";
import { styles } from "./StoreCredit.style";

// interface
import { IStoreCredits } from "./IStoreCredits";

// hooks
import { useStoreCredits } from "../../context/StoreCredits/storeCredits-context";
import { useNavigation } from "@react-navigation/native";

// services
import storeCreditService from "../../services/storeCredits/storeCredits";

// compoents
import UpdateStoreCredits from "../popups/updateModals/updateStoreCredits";

export const StoreCredit: FC<IStoreCredits> = ({ id, name, store, amount, expirationDate: expirationDate, image, purchaseDate }) => {

    const { storeCredits, setStoreCredits } = useStoreCredits();

    const navigation = useNavigation();

    const handleStoreCreditDelete = () => {
        setStoreCredits([...storeCredits].filter((element) => element.id !== id));
        storeCreditService.deleteById(+id)
        navigation.goBack();
    };

    return (
        <View style={cardStyles.cardBackground}>

            {/* TODO: Need to display store credit img */}

            <Text style={cardStyles.property}>name: {name}</Text>
            <Text style={cardStyles.property}>store: {store}</Text>
            <Text style={cardStyles.property}>amount: {amount}</Text>
            <Text style={cardStyles.property}>Experation date: {new Date(expirationDate).toLocaleDateString()}</Text>
            <Text style={cardStyles.property}>Purchase date: {new Date(purchaseDate).toLocaleDateString()}</Text>
            <UpdateStoreCredits name={name} id={id} store={store} amount={amount} expirationDate={expirationDate} image={image} />
            <Pressable
                style={[styles.button, styles.buttonAdd]}
                onPress={() => handleStoreCreditDelete()}
            >
                <Text style={styles.textStyle}>Delete</Text>
            </Pressable>
        </View>
    );
};
