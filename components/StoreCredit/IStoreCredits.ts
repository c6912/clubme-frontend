import { CardProps } from "../PopupCard/PopupCard.types";

export interface IStoreCredits extends CardProps {
  purchaseDate: string;
  expirationDate: string;
  amount: number;
  image: string;
}

export interface IStoreCreditsCategories {
  id: number;
  storecredit_id: number;
  category_id: number;
}

export interface IStoreCreditsUser {
  id: number;
  storecredit_id: number;
  user_id: number;
  isFavorite: boolean;
}

