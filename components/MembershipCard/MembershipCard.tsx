import React, { FC } from "react";

// style
import { Pressable, Text, View } from "react-native";
import { cardStyles } from "../PopupCard/PopupCard.style";
import { styles } from "../StoreCredit/StoreCredit.style";

// components
import UpdateMembershipCardModal from "../popups/updateModals/updateMembershipCardModal";

// hooks
import { useNavigation } from "@react-navigation/native";
import { useMembershipCards } from "../../context/MembershipCards/membershipcards-context";

// services
import membershipCardService from "../../services/memberships/memberships";

// interfaces
import { IMembership } from "./Membership.types";

export const MembershipCard: FC<IMembership> = ({
	id,
	expDate,
	points,
	purchaseDate,
	name,
	store,
}) => {
	const { membershipCards, setMembershipCards } = useMembershipCards();
	const navigation = useNavigation();

	const handleDelete = () => {
		let tempMembershipCards = [...membershipCards];
		tempMembershipCards = tempMembershipCards.filter(
			(element) => element.id !== id
		);
		setMembershipCards(tempMembershipCards);
		membershipCardService.deleteById(+id);
		navigation.goBack();
	};

	return (
		<View style={cardStyles.cardBackground}>
			<Text style={cardStyles.property}>Points: {points}</Text>
			<Text style={cardStyles.property}>
				Purchase date: {new Date(purchaseDate).toLocaleDateString()}
			</Text>
			<Text style={cardStyles.property}>
				Experation date: {new Date(expDate).toLocaleDateString()}
			</Text>
			<UpdateMembershipCardModal
				expDate={expDate}
				id={id}
				name={name}
				store={store}
				points={points}
			/>
			<Pressable
				style={[styles.button, styles.buttonAdd]}
				onPress={() => handleDelete()}
			>
				<Text style={styles.textStyle}>Delete</Text>
			</Pressable>
		</View>
	);
};
