import { CardProps } from "../PopupCard/PopupCard.types";

export interface IMembership extends CardProps {
	purchaseDate: string;
	expirationDate: string;
	points: number;
}
