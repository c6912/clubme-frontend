import { IMembership } from "./Membership.types";

export const memberShipCard1: IMembership = {
    id: "12",
    name: "Dream Card - 'Fox'",
    store: "Fox",
    companyId: 24,
    expDate: new Date(),
    points: 12,
    purchaseDate: new Date(),
    storeTags: ['Furniture']

};

export const memberShipCard2: IMembership = {
    id: "9",
    name: "Dream Card - 'Catro'",
    store: "Castro",
    companyId: 24,
    expDate: new Date(),
    points: 223,
    purchaseDate: new Date(),
    storeTags: ['Furniture']

};

export const memberShipCard3: IMembership = {
    id: "7",
    name: "Dream Card - 'Honigman'",
    store: "Honigman",
    companyId: 24,
    expDate: new Date(),
    points: 350,
    purchaseDate: new Date(),
    storeTags: ['Food']

};

export const memberShipCard4: IMembership = {
    id: "1",
    name: "Dream Card - 'Fox'",
    store: "Fox",
    companyId: 5,
    expDate: new Date(),
    points: 77,
    purchaseDate: new Date(),
    storeTags: ['Vacation']

};
export const memberShipCard5: IMembership = {
    id: "4",
    name: "Dream Card - 'AE'",
    store: "AE",
    companyId: 2,
    expDate: new Date(),
    points: 21,
    purchaseDate: new Date(),
    storeTags: ['Sports']

};
export const memberShipCard6: IMembership = {
    id: "2",
    name: "Dream Card - 'H&M'",
    store: "H&M",
    companyId: 24,
    expDate: new Date(),
    points: 174,
    purchaseDate: new Date(),
    storeTags: ['Vacation']

};


export const memberShipCards: IMembership[] = [
    memberShipCard1,
    memberShipCard2,
    memberShipCard3,
    memberShipCard4,
    memberShipCard5,
    memberShipCard6,
];
