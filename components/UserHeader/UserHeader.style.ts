import { Dimensions, StyleSheet } from "react-native";

export const styles = StyleSheet.create({
	container: {
		width: Dimensions.get("window").width - 30,
		flexDirection: "row",
		alignContent: "center",
		display: "flex",
		alignItems: "center",
		marginLeft: 5
	},
	fullName: {
		fontSize: 18,
		 marginLeft: 10
	}
});
