import { FontAwesome } from "@expo/vector-icons";
import React, { useEffect, useState } from "react";
import { View } from "react-native";
import userService from "../../services/common-beckend/userService";
import { getUser } from "../../utils/token";
import { NunText } from "../StyledText";
import { styles } from "./UserHeader.style";
export function UserHeader() {

    const [fullName, setFullName] = useState<string>("");

    useEffect(() => {
        getUser().then(({ username }) => {
            userService
                .find(username)
                .then(({ firstName, lastName }: any) => {
                    setFullName(`${firstName} ${lastName}`);
                })
                .catch((error) => {
                    console.log({ error });
                });
        });
    }, []);

    return (
        <View style={styles.container}>
            <FontAwesome
                size={26}
                name="user"
            />
            <NunText style={styles.fullName}>
                {fullName}
            </NunText>
        </View >
    );
}
