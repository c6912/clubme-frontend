import { StyleSheet } from "react-native";

export const cardStyles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
	},
	card: {
		borderRadius: 10,
		width: 300,
		maxHeight: 500,
		// elevation: 6,
		// shadowColor: 'black',
	},
	cardBackground: {
		backgroundColor: "#dddede",
	},
	cardTitle: {
		fontSize: 25,
		fontFamily: "nunito-bold",
		marginBottom: 10,
		textAlign: "center",
	},
	property: {
		marginBottom: 10,
		fontSize: 20,
		fontFamily: "nunito",
	},
	button: {
		borderRadius: 5,
		marginTop: 20,
		width: "30%",
		alignSelf: "center",
	},
	buttonIcon: {
		marginLeft: 10,
	},
});
