import { GiftCardProps } from "../GiftCard/GiftCard.types";
import { IMembership } from "../MembershipCard/Membership.types";
import { IStoreCredits } from "../StoreCredit/IStoreCredits";

// common cards properties - from DB schema
export interface CardProps {
	id: string;
	mainUserId: number;
	name: string;
	store: string;
	isFavorite: boolean;
	categories: boolean[];
	onClose?: () => void;
	ChildComp?: React.ReactNode;
}

export const CardTypes = {
	GiftCard: "giftcard",
	MembershipCard: "membershipCard",
	StoreCredit: "storeCredit",
} as const;

type CardTypesKeys = keyof typeof CardTypes;
export type CardType = typeof CardTypes[CardTypesKeys];

export type Item = (IStoreCredits | IMembership | GiftCardProps) & {
	type: string;
	isFavorite: boolean;
	shared: boolean;
};
