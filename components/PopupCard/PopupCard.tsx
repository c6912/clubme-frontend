import { useRoute } from "@react-navigation/native";
import React, { FC, ReactElement, useState } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Card } from "react-native-elements";
import { GiftCard } from "../GiftCard/GiftCard";
import { MembershipCard } from "../MembershipCard/MembershipCard";
import ShareModal from "../popups/shareModal";
import { cardStyles } from "./PopupCard.style";
import { CardProps, CardType, CardTypes } from "./PopupCard.types";

const PopupCard: FC<CardProps> = () => {
	const { params } = useRoute();
	const [cardData] = useState<CardProps>(params as CardProps);

	const getCardTypeByChildType = (childType: any) =>
		childType == GiftCard
			? CardTypes.GiftCard
			: childType == MembershipCard
			? CardTypes.MembershipCard
			: CardTypes.StoreCredit;

	const [cardType] = useState<CardType>(
		getCardTypeByChildType((cardData?.ChildComp as ReactElement).type)
	);

	return (
		<View style={cardStyles.container}>
			<Card containerStyle={[cardStyles.card, cardStyles.cardBackground]}>
				<Text style={cardStyles.cardTitle}>{cardData.name}</Text>
				<Card.Divider color="black" />
				{cardData.image && (
					<Image
						style={styles.img}
						source={{
							uri: cardData.image,
						}}
					/>
				)}

				{cardData.ChildComp}

				<ShareModal
					cardName={cardData.name}
					cardId={cardData.id}
					cardType={cardType}
				></ShareModal>
			</Card>
		</View>
	);
};

const styles = StyleSheet.create({
	input: {
		height: 40,
		width: 200,
		margin: 12,
		borderWidth: 1,
		padding: 10,
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		marginTop: 22,
	},
	modalView: {
		margin: 20,
		backgroundColor: "white",
		borderRadius: 20,
		padding: 35,
		alignItems: "center",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		elevation: 5,
	},
	button: {
		borderRadius: 20,
		padding: 10,
		marginLeft: 20,
		marginRight: 20,
		elevation: 2,
	},
	buttonOpen: {
		backgroundColor: "#F194FF",
	},
	buttonAdd: {
		backgroundColor: "green",
	},
	buttonCancel: {
		backgroundColor: "red",
	},
	textStyle: {
		color: "white",
		fontWeight: "bold",
		textAlign: "center",
	},
	modalText: {
		marginBottom: 15,
		textAlign: "center",
		fontSize: 20,
		fontWeight: "bold",
	},
	img: {
		width: "auto",
		height: 100,
	},
});

export default PopupCard;
