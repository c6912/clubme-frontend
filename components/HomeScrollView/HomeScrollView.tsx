import { BoldNunText } from "../StyledText";
import React, { useEffect, useRef } from "react";
import { ScrollView, View, Text, Dimensions } from "react-native";
import { styles } from "./HomeScrollView.style";

export const HomeScrollView = ({
	children,
	snapToInterval,
	title,
}: {
	children: React.ReactNode;
	snapToInterval: number;
	title: string;
}) => {
	const scrollViewRef = useRef<ScrollView>(null);

	useEffect(() => {
		setTimeout(() => {
			scrollViewRef.current?.scrollTo({ x: -30 });
		}, 1); // scroll view position fix
	}, []);
	return (
		<View style={styles.favorites}>
			<BoldNunText style={styles.favoritesTitle}>{title}</BoldNunText>
			<ScrollView
				ref={scrollViewRef}
				horizontal={true}
				decelerationRate={0}
				snapToInterval={snapToInterval}
				snapToAlignment={"center"}
				contentInset={{
					top: 0,
					left: 30,
					bottom: 0,
					right: 30,
				}}
			>
				{children}
			</ScrollView>
		</View>
	);
};
