import { Dimensions, StyleSheet } from "react-native";

const { width } = Dimensions.get("window");

export const styles = StyleSheet.create({
	// container: {
	// 	flex: 1,
	// 	alignItems: "center",
	// 	justifyContent: "center",
	// },
	title: {
		fontSize: 20,
		fontWeight: "bold",
	},
	separator: {
		marginVertical: 30,
		height: 1,
		width: "80%",
	},
	container: {},
	favoritesTitle: {
		fontSize: 24,
		fontWeight: "400"
	},
	view: {
		// marginTop: 100,
		backgroundColor: "blue",
		width: width - 80,
		margin: 10,
		height: 200,
		borderRadius: 10,
		//paddingHorizontal : 30
	},
	view2: {
		// marginTop: 100,
		backgroundColor: "red",
		width: width - 80,
		margin: 10,
		height: 200,
		borderRadius: 10,
		//paddingHorizontal : 30
	},
	favorites: { marginTop: 22, marginHorizontal: 20, },
});
