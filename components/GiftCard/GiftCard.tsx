import React, { FC, ReactElement, useEffect, useState } from "react";
import { StyleSheet, Text, Pressable, View } from "react-native";
import { useGiftCards } from "../../context/giftCards/giftcards-context";
import { GiftCardProps } from "../GiftCard/GiftCard.types";
import { cardStyles } from "../PopupCard/PopupCard.style";
import { useNavigation } from "@react-navigation/native";
import { MembershipCard } from "../MembershipCard/MembershipCard";
import ShareModal from "../popups/shareModal";
import { CardType, CardTypes } from "../PopupCard/PopupCard.types";

import giftcardService from "../../services/common-beckend/giftcardService";

export const GiftCard: FC<GiftCardProps> = ({
	expirationDate,
	id,
	name,
	store,
	amount,
	ChildComp,
}) => {
	const getCardTypeByChildType = (childType: any) =>
		childType == GiftCard
			? CardTypes.GiftCard
			: childType == MembershipCard
			? CardTypes.MembershipCard
			: CardTypes.StoreCredit;

	const [cardType] = useState<CardType>(
		getCardTypeByChildType((ChildComp as ReactElement).type)
	);

	const { giftCards, setGiftCards } = useGiftCards();
	const navigation = useNavigation();

	useEffect(() => {
		console.log({ expirationDate });
	}, [giftCards]);

	const handleDelete = () => {
		let tempGiftCardItems = [...giftCards];
		tempGiftCardItems = tempGiftCardItems.filter(
			(element) => element.id !== id
		);

		giftcardService.delete(id)

		setGiftCards(tempGiftCardItems);
		navigation.goBack();
	};

	return (
		<View style={cardStyles.cardBackground}>
			<Text style={cardStyles.property}>{store}</Text>
			<Text style={cardStyles.property}>{amount}</Text>
			<Text style={cardStyles.property}>
				Experation date: {new Date(expirationDate).toLocaleDateString()}
			</Text>
			<Pressable
				style={[styles.button, styles.buttonAdd]}
				onPress={() => handleDelete()}
			>
				<Text style={styles.textStyle}>Delete</Text>
			</Pressable>
			<ShareModal cardName={name} cardId={id} cardType={cardType} />

			{/* <PopupCard /> */}
			{/* `			<UpdateGiftCardModal
				balance={balance}
				expDate={expDate}
				id={id}
				name={name}
				store={store}
				value={value}
				storeTags={storeTags}
			/>` */}
		</View>
	);
};

const styles = StyleSheet.create({
	input: {
		height: 40,
		width: 200,
		margin: 12,
		borderWidth: 1,
		padding: 10,
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		marginTop: 22,
	},
	modalView: {
		margin: 20,
		backgroundColor: "white",
		borderRadius: 20,
		padding: 35,
		alignItems: "center",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		elevation: 5,
	},
	button: {
		borderRadius: 20,
		padding: 10,
		marginLeft: 20,
		marginRight: 20,
		elevation: 2,
	},
	buttonOpen: {
		backgroundColor: "#F194FF",
	},
	buttonAdd: {
		backgroundColor: "red",
	},
	buttonCancel: {
		backgroundColor: "red",
	},
	textStyle: {
		color: "white",
		fontWeight: "bold",
		textAlign: "center",
	},
	modalText: {
		marginBottom: 15,
		textAlign: "center",
		fontSize: 20,
		fontWeight: "bold",
	},
});
