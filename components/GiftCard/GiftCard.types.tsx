import { CardProps } from "../PopupCard/PopupCard.types";

export interface GiftCardProps extends CardProps {
	purchaseDate: string;
	expirationDate: string;
	amount: number;
}
