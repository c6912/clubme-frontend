import { GiftCardProps } from "./GiftCard.types";

export const giftCard1: GiftCardProps = {
	id: "1234",
	name: "Dream Card - 'Fox'",
	store: "Castro",
	balance: 50,
	value: 450,
	storeTags: ["Fashion"],
	isFavorite: false,
	date: new Date().toLocaleDateString(),
};

export const giftCard2: GiftCardProps = {
	id: "1434",
	name: "Dream Card - 'Fox Home'",
	store: "Mega Sport",
	balance: 250,
	value: 450,
	storeTags: ["Fashion", "Furniture"],
	isFavorite: false,
	date: new Date().toLocaleDateString(),
};

export const giftCard3: GiftCardProps = {
	id: "244",
	name: "Dream Card - 'Mango'",
	store: "Be",
	balance: 250,
	value: 450,
	storeTags: ["Fashion"],
	isFavorite: false,
	date: new Date().toLocaleDateString(),
};

export const giftCard4: GiftCardProps = {
	id: "4577",
	name: "Dream Card - 'American Eagle'",
	store: "Home design",
	balance: 250,
	value: 450,
	storeTags: ["Fashion"],
	isFavorite: false,
	date: new Date().toLocaleDateString(),
};

export const giftCard5: GiftCardProps = {
	id: "25678",
	name: "Dream Card - 'Aerie'",
	store: "Home design",
	balance: 250,
	value: 450,
	storeTags: ["Fashion"],
	isFavorite: false,
	date: new Date().toLocaleDateString(),
};

export const giftCard6: GiftCardProps = {
	id: "9876",
	name: "Dream Card -  'The children's place'",
	store: "Pizza Hut",
	balance: 250,
	value: 450,
	storeTags: ["Fashion"],
	isFavorite: false,
	date: new Date().toLocaleDateString(),
};

export const giftCards: GiftCardProps[] = [
	giftCard1,
	giftCard2,
	giftCard3,
	giftCard4,
	giftCard5,
	giftCard6,
];
