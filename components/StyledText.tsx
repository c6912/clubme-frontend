import React from 'react';
import { Text, TextProps } from './Themed';

export function BoldNunText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontFamily: 'nunito-bold' }]} />;
}

export function NunText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontFamily: 'nunito' }]} />;
}
