import React from "react";
import { View } from "react-native";
import { Image } from "react-native-elements";
import NotificationsModal from "../popups/notificationsModal";
import { styles } from "./HomeHeader.style";
import logo from "../../assets/images/logo.jpeg";

export function HomeHeader() {
	return (
		<View style={styles.container}>
			<Image style={styles.logo} source={logo} />
			<NotificationsModal />
		</View>
	);
}
