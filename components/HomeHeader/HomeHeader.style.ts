import { Dimensions, StyleSheet } from "react-native";

export const styles = StyleSheet.create({
	container: {
		width: Dimensions.get("window").width - 30,
		flexDirection: "row",
		alignContent: "center",
		justifyContent: "space-between",
		display: "flex",
		alignItems: "center",
	},
	baseText: {
		fontWeight: "bold",
	},
	innerText: {
		color: "red",
	},
	logo: {
		width: 85,
		height: 60,
	},
});
