import React, { useState } from "react";
import { Text, View, TouchableOpacity } from "react-native";

export const CardsTabs = ({ tabs, onTabChanged = () => {} }: any) => {
	const [activeTab, setActiveTab] = useState(0);
	return (
		<View
			style={{
				display: "flex",
				flexDirection: "row",
				justifyContent: "space-around",
				width: "100%",
				borderBottomColor: "gray",
				borderBottomWidth: 1,
				// height: 40,
			}}
		>
			{tabs.map(({ name }: any, key: number) => {
				const style =
					key === activeTab
						? { borderBottomColor: "black", borderBottomWidth: 3, padding: 10 }
						: { padding: 10 };
				return (
					<View
						key={key}
						style={style}
						// onClick={() => {

						// }}
					>
						<TouchableOpacity
							onPress={() => {
								if (activeTab !== key) {
									setActiveTab(key);
									onTabChanged(key);
								}
							}}
						>
							<Text>{name}</Text>
						</TouchableOpacity>
					</View>
				);
			})}
		</View>
	);
};
