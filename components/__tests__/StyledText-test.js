import * as React from 'react';
import renderer from 'react-test-renderer';

import { BoldNunText } from '../StyledText';

it(`renders correctly`, () => {
  const tree = renderer.create(<BoldNunText>Snapshot test!</BoldNunText>).toJSON();

  expect(tree).toMatchSnapshot();
});
