import React, { useState, useRef } from "react";
import { View } from "react-native";
import { Modal, Portal, Text, Button, Provider } from "react-native-paper";
export const Dropdown = () => {
	const [visible, setVisible] = useState(false);
	const dropdownRef = useRef<any>(null);

	const showModal = () => setVisible(true);
	const hideModal = () => setVisible(false);
	const containerStyle = {
		backgroundColor: "white",
		padding: 20,
		zIndex: 2500,
		position: "absolute",
		width: "100%",
		top: `${dropdownRef?.current ? dropdownRef.current.clientHeight : 0}px`,
	};
	console.log(dropdownRef?.current?.offsetTop);

	return (
		<View style={{ width: "100%" }}>
			{visible && (
				<View
					onClick={() => {
						hideModal();
					}}
					style={{
						backgroundColor: "white",
						padding: 20,
						zIndex: 2500,
						position: "absolute",
						width: "100%",
						height: "20px",
						top: `${
							dropdownRef?.current ? dropdownRef.current.clientHeight : 0
						}px`,
						borderRadius: 10,
						borderColor: "black",
						borderWidth: 2,
					}}
				>
					<Text>dsd</Text>
				</View>
			)}
			<div ref={dropdownRef}>
				<Button
					style={{
						marginTop: 30,
						borderRadius: 10,
						borderColor: "black",
						borderWidth: 2,
					}}
					onPress={() => {
						console.log(dropdownRef.current.offsetTop);

						showModal();
					}}
				>
					Show
				</Button>
			</div>
		</View>
	);
};
