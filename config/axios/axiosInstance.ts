import axios from "axios";
import type {
	AxiosRequestConfig,
	AxiosRequestHeaders,
	AxiosInstance,
} from "axios";
import { IAxiosConfig } from "./IAxios";

export const createAxiosInstance = ({
	serviceBaseUrl,
	prefix,
	headers,
}: IAxiosConfig): AxiosInstance => {
	const axiosInstance = axios.create({ baseURL: serviceBaseUrl + prefix });
	// axiosInstance.interceptors.request.use(injectToken(headers));
	return axiosInstance;
};

// const injectToken =
//   (headers?: AxiosRequestHeaders) => async (config: AxiosRequestConfig) => {
//     const idToken = await getToken();
//     idToken
//       ? () => {
//         config.headers = {
//           Authorization: `Bearer ${idToken}`,
//           "Content-Type": "application/json",
//         };
//         headers && Object.assign(config.headers, headers);
//       }
//       : console.error("failed to get access token");

//     return config;
//   };
