import AsyncStorage from "@react-native-async-storage/async-storage";

export const storeUser = async (username: string, token: string) => {
	try {
		const user = JSON.stringify({ username, token });
		await AsyncStorage.setItem("user", user);
	} catch (e) {
		// saving error
	}
};

export const getUser = async () => {
	try {
		const user = await AsyncStorage.getItem("user");
		return user != null ? JSON.parse(user) :  { token: '' };
	} catch (e) {
		return { token: '' }
	}
};

export const deleteUser = async () => {
	try {
		return await AsyncStorage.removeItem("user");
	} catch (e) {
		// error reading value
	}
};
