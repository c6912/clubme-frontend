import { createContext, useContext, useEffect, useState, FC } from "react";

import { memberShipCards } from "../../components/MembershipCard/Membership.mock";

// interfaces
import { IMembershipContext } from "./IMembershipCards";
import { IMembership } from "../../components/MembershipCard/Membership.types";

// services
import membershipsCardService from "../../services/memberships/memberships";

import { getUser } from "../../utils/token";

export const ThemeContext = createContext<IMembershipContext>(
	{} as IMembershipContext
);

export const useMembershipCards = (): IMembershipContext =>
	useContext(ThemeContext);

export const MembershipCardProvider: FC = ({ children }) => {
	const [membershipCards, setMembershipCards] = useState<IMembership[]>([]);

	const value: IMembershipContext = {
		membershipCards,
		setMembershipCards,
	};
	return (
		<ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>
	);
};
