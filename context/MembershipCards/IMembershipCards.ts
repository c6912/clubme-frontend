// interfaces

import { IMembership } from "../../components/MembershipCard/Membership.types"


export interface IMembershipContext {
    membershipCards: IMembership[]
    setMembershipCards: React.Dispatch<React.SetStateAction<IMembership[]>>

}

