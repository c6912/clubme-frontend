// interfaces

import { GiftCardProps } from "../../components/GiftCard/GiftCard.types"


export interface IGiftCards {
    giftCards: GiftCardProps[]
    setGiftCards: React.Dispatch<React.SetStateAction<GiftCardProps[]>>

}

