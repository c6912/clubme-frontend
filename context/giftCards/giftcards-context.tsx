import { createContext, useContext, useState, FC } from "react";
import { IGiftCards } from "./IGiftCard";

import { giftCards as giftCardsData } from "../../components/GiftCard/GiftCard.mock";
import { GiftCardProps } from "../../components/GiftCard/GiftCard.types";

export const ThemeContext = createContext<IGiftCards>({} as IGiftCards);

export const useGiftCards = (): IGiftCards => useContext(ThemeContext);

export const GiftCardsProvider: FC = ({ children }) => {
	const [giftCards, setGiftCards] = useState<GiftCardProps[]>([]);

	const value: IGiftCards = {
		giftCards,
		setGiftCards,
	};
	return (
		<ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>
	);
};
