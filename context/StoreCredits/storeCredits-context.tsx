import { createContext, useContext, useState, FC, useEffect } from "react";

import { StoreCreditMock } from "../../components/StoreCredit/StoreCredits.mock";

// interfaces
import { IStoreCredits } from "../../components/StoreCredit/IStoreCredits";
import { IContextStoreCredits } from "./IContextStoreCredits";

import storeCreditsService from "../../services/storeCredits/storeCredits";

import { getUser } from "../../utils/token";

export const ThemeContext = createContext<IContextStoreCredits>(
	{} as IContextStoreCredits
);

export const useStoreCredits = (): IContextStoreCredits =>
	useContext(ThemeContext);

export const StoreCreditsProvider: FC = ({ children }) => {
	const [storeCredits, setStoreCredits] = useState<IStoreCredits[]>([]);

	const value: IContextStoreCredits = {
		storeCredits,
		setStoreCredits,
	};
	return (
		<ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>
	);
};
