import { IStoreCredits } from "../../components/StoreCredit/IStoreCredits"

export interface IContextStoreCredits {
    storeCredits: IStoreCredits[]
    setStoreCredits: React.Dispatch<React.SetStateAction<IStoreCredits[]>>

}