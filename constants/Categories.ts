export const CATEGORIES = [
	"Fashion",
	"Sports",
	"Furniture",
	"Footwear",
	"Food",
	"Vacation",
];

export const CARD_BELONGING_TYPES = {
	MINE: { id: 0, name: "MINE" },
	SHARED: { id: 1, name: "SHARED" },
	EXTERNAL: { id: 2, name: "EXTERNAL" },
};
