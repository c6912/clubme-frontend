export const SIGN_UP_FIELDS = {
	EMAIL_ADDRESS: "emailAddress",
	PASSWORD: "password",
	FIRST_NAME: "firstName",
	LAST_NAME: "lastName",
	DATE_OF_BIRTH: "dateOfBirth",
	GENDER: "gender",
};
