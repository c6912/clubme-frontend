export enum GENDER {
	NONE,
	MALE,
	FEMALE,
	OTHER,
}
